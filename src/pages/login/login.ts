import { User } from './../../model/user.model';
import { Observable } from 'rxjs/Observable';
import { RegisterPage } from '../Register/register';
import { Component, ViewChild } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { AngularFireDatabase } from 'angularfire2/database';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { NavController,LoadingController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { ForgotPage } from '../forgot/forgot';
import * as firebase from 'firebase';
import { firebaseConfig } from '../../app/app.module';
import { GooglePlus } from '@ionic-native/google-plus';
import { environment } from '../../environments/environment';
import { Url, DataServiceProvider } from '../../providers/data-service/data-service'
import { Facebook } from '@ionic-native/facebook';
import { APIResponse } from '../../model/response.model';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  @ViewChild('email') email: any;
  public username: string;
  public password: string;
  public error: string;
  apiUrl = 'http://72.249.170.12/BluetoothApi/api/Login/Login';
  passwordType: string = 'password';
  paIcon: string = 'eye-off';
  user: Observable<User[]>;
  loginError: string;
  uber: Observable<any>;
  constant : Url;

  public loading;
  constructor(private firebaseProvider: FirebaseProvider,
    private navCtrl: NavController,
    public http: Http,
    public gPlus: GooglePlus,
    public afDatabase: AngularFireDatabase,
    public api: DataServiceProvider,
    public url: Url,
    public facebook: Facebook,
    public loadingCtrl: LoadingController

  ) {
    //this.user = this.firebaseProvider.getShoppingItems();    
     this.loading = this.loadingCtrl.create({ content: "please wait..." });

  }
  //https://www.djamware.com/post/59924f9080aca768e4d2b12e/ionic-3-consuming-rest-api-using-new-angular-43-httpclient

  ionViewDidLoad(): void {
    setTimeout(() => {
      this.email.setFocus();
    }, 500);
  }

  login(): void {
    this.loading.present();
    let obj = {
      email: this.username,
      password: this.password
    };
    this.api.postRequset(this.url.login, obj).subscribe((response_api:APIResponse)=> {
        // var response_api  = new APIResponse(response); 
           this.loading.dismiss();
           if(response_api.status===this.constant.ok)
            {
             this.constant.user=JSON.parse(response_api.data).appuser;
             alert("success: "+this.constant.user.email);
            }
            else{
              alert("Failed: "+response_api.errors[0].code);
            }
    },
      error => { 
        this.loading.dismiss();
        alert("error"+error) ;
    });
  }

  loginFirebase() {
    debugger;
    if (!this.username) {
      return;
    }
    let credentials = {
      email: this.username,
      password: this.password
    };
    this.firebaseProvider.signInWithEmail(credentials)
      .then(
        (user) => {
          if (user.user.emailVerified) {
            this.firebaseProvider.Superuser = user.user;
            var pre_user = user.user;
            if (pre_user) {
              this.firebaseProvider.findEmail(pre_user.email).then(
                (ref) => {
                  this.uber = ref;
                  ////
                  this.uber.subscribe((res: User[]) => {
                    console.log(res);
                    var m_user = res
                    if (m_user.length === 0) {
                      this.firebaseProvider.isRegistredUser = false;
                      this.navCtrl.setRoot(RegisterPage, { "email": pre_user.email });
                    }
                    else {

                      this.firebaseProvider.username = res[0].Username;
                      this.navCtrl.setRoot(HomePage);
                    }
                  },
                    () => {
                      this.firebaseProvider.isRegistredUser = false;
                      return false;
                    }
                  );

                }
              );
            }

            ////////////////
          }
          else {
            alert("Email not varifired.Please check your mailbox");
            this.firebaseProvider.sendEmailVerificationLink(user.user).then(
              (ref) => {
              },
              (error) => {
                alert(error.message);
              }
            );
          }
        },
        error => { alert(error.message); this.loginError = error.message }

      );
  }


  signup() {
    this.navCtrl.push(RegisterPage, { "email": "null" });
  }
  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.paIcon = this.paIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  forgotPassword() {
    this.navCtrl.push(ForgotPage);
  }

  async loginWithGooglePlus() {
    // alert("succed");
    this.gPlus.login({
      'scopes': '', // optional - space-separated list of scopes, If not included or empty, defaults to `profile` and `email`.
      'webClientId': environment.googleWebClientId, // optional - clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.
      'offline': true, // Optional, but requires the webClientId - if set to true the plugin will also return a serverAuthCode, which can be used to grant offline access to a non-Google server
    }).then(user => {
      //save user data on the native storage\
      alert("succed");
      // this.nativeStorage.setItem('google_user', {
      //   name: user.displayName,
      //   email: user.email,
      //   picture: user.imageUrl         
      // }).then(() => {

      // }, (error) => {
      //   console.log(error);
      // })
    }, err => {
      alert("failed" + err);
    });
  }



  // Function for Sigin in With Google using FirebaseAuth
  loginWithGoogle() {
    this.firebaseProvider.signInWithGoogle()
      .then(
        (user) => {
         
          if (user.user.emailVerified) {
            this.firebaseProvider.Superuser = user.user;
            var pre_user = user.user;
            if (pre_user) {
              this.firebaseProvider.findEmail(pre_user.email).then(
                (ref) => {
                  this.uber = ref;
                  ////
                  this.uber.subscribe((res: User[]) => {
                    console.log(res);
                    var m_user = res
                    if (m_user.length === 0) {
                      this.firebaseProvider.isRegistredUser = false;
                      this.navCtrl.setRoot(RegisterPage, { "email": pre_user.email });
                    }
                    else {
                      this.firebaseProvider.username = res[0].Username;
                      this.navCtrl.setRoot(HomePage);
                    }
                  },
                    () => {
                      this.firebaseProvider.isRegistredUser = false;
                      return false;
                    }
                  );

                }
              );
            }
          }
          else {
            alert("Email not varifired.Please check your mailbox");
          }
        },
        (error) => console.log(error.message)
      );
  }

  loginWithFacebook() {
    this.facebook.login(['public_profile', 'user_friends', 'email'])
      .then(res => {
        if (res.status === "connected") {
          // this.isLoggedIn = true;
          this.facebook.api("/" + res.authResponse.userID + "/?fields=id,email,name,picture,gender", ["public_profile"])
            .then(res => {
              console.log(res);
              alert("succes"+res);
             // this.users = res;
            })
            .catch(e => {
              alert("exception"+e);
              console.log(e);
            });

        } else {
          alert("failed"+res);
         // this.isLoggedIn = false;
        }
      })
      .catch(e => console.log('Error logging into Facebook', e));
  }


  saveData() {
    const obj = {
      UserName: "N/A",
      Password: "N/A",
      Name: "N/A",
      Email: this.firebaseProvider.getEmail()
    }
    this.firebaseProvider.addUser(obj);
  }



}
