import { HomePage } from '../home/home';
import { LoginPage } from '../login/login';
import { User } from '../../model/user.model';
// import {HttpParams} from '@angular/common/http/src/params';
// import { Component, ViewChild } from '@angular/core';
// import { Http, Headers,  } from '@angular/http';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Component } from '@angular/core';
// kimport {HttpParams} from '@angular/common/http/src/params';
//import { AlertController } from '../../../node_modules/ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { NavController, NavParams ,LoadingController} from 'ionic-angular';
import { Url,DataServiceProvider }  from  '../../providers/data-service/data-service'

import { Observable } from 'rxjs/Observable';
import { APIResponse } from '../../model/response.model';

@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
@Injectable()
export class RegisterPage {

  public Firstname: string = "";
  public Lastname: string = "";
  public isCoach: boolean = true;
  public Password: string = "";
  public Password_2: string = "";
  public Email: string = "";
  public Phone: string = "";
  public u_email: string = "";
  public flag: boolean = false;
  public pre_user: firebase.User
  public uber: Observable<User[]>;
  public m_user: User[];
  public loading;
  constant : Url;


  signupError: string;
  // private error: string;
  private user: Observable<any[]>;
  apiUrl = 'http://72.249.170.12/BluetoothApi/api/Login/Login';
  constructor(public navParams: NavParams,
    private navCtrl: NavController,
    public http: Http,
    public firebaseProvider: FirebaseProvider,
    public api : DataServiceProvider,
    public url : Url,
    public loadingCtrl: LoadingController

    ) {
      this.loading = this.loadingCtrl.create({ content: "please wait..." });

    //  if(navParams.get("email") && navParams.get("email")!=="null")  {
    //   debugger;
    //   this.u_email = this.firebaseProvider.Superuser.email.toLowerCase();
    //   this.Email = this.u_email;
    //   this.Password="null";
    //   if(this.u_email && this.u_email.length!==0){
    //     this.flag= true;
    //   }

    // }

  }

  signup(): void {
    this.loading.present();
    let obj = { FirstName : this.Firstname ,
                LastName: this.Lastname ,
                Email: this.Email.toLowerCase() ,
                Password: this.Password ,
                RoleId : this.isCoach ? "1" : "2",
                PhoneNumber: this.Phone
              };              
         
    this.api.postRequset(this.url.register ,obj).subscribe( (response_api:APIResponse) =>{
      this.loading.dismiss();
      if(response_api.status===this.constant.ok)
       {
         alert("SuccessToken: "+response_api.data);
       }
       else{
         alert("Failed: "+response_api.errors[0].code);
       }
       },
         error => { 
        this.loading.dismiss();
        alert("error"+error) ;
    }); 
  }

  //https://www.djamware.com/post/59924f9080aca768e4d2b12e/ionic-3-consuming-rest-api-using-new-angular-43-httpclient

  // ionViewDidLoad(): void {
  //   setTimeout(() => {
  //     this.email.setFocus();
  //   }, 500);
  // }

  // register(): any {
  //   const obj = {
  //     UserName:this.Username ,
  //     Password:this.password,
  //     Name:this.Name,
  //     Email:this.Email
  //   }
  //   var headers = new Headers();
  //   headers.set('Content-type','application/json');
  //     console.log('doing'+ JSON.stringify(obj));
  //     debugger;
  //      const uri = 'http://72.249.170.12/BluetoothApi/api/Login/Login';
  //      this.http.post(uri,JSON.stringify(obj),{headers})
  //     .subscribe(res => {
  //       console.log("done");
  //       alert('done');
  //     }, (err) => {
  //       console.log(err);
  //     });
  // }

  // addUSer(){
  //   var obj : User;
  //    obj = new User();
  //    if(this.flag)
  //   {
  //     obj.Email= this.u_email;
  //     obj.LoginType= "gmail";
  //     obj.Password= "null";
  //   }
  //    else
  //   {
  //     obj.Email= this.u_email;
  //     obj.LoginType= "email";
  //     obj.Password= this.password;

  //   }
  //    obj.Name= this.Name;
  //    obj.Username= this.Username;

  //   this.firebaseProvider.addUser(obj);
  // }

  //{}
  // newSongRef.set({
  //   id: newSongRef.key,
  //   title: data.title
  // });
  // let prompt = this.alertCtrl.create({
  //   title: 'Submit the details',
  //   message: "Are you confirmed to submit ? ",
  //   inputs: [
  //     {
  //       name: 'title',
  //       placeholder: 'Title'
  //     },
  //   ],
  //   buttons: [
  //     {
  //       text: 'Cancel',
  //       handler: data => {
  //         console.log('Cancel clicked');

  //       }
  //     },
  //     {
  //       text: 'Save',

  //       handler: data => {
  //         const obj = {
  //           UserName:this.Username ,
  //           Password:this.password,
  //           Name:this.Name,
  //           Email:this.Email
  //         }
  //         const newSongRef = this.user.push(obj);
  //          console.log(""+newSongRef.key+","+data.title);
  //         // newSongRef.set({
  //         //   id: newSongRef.key,
  //         //   title: data.title
  //         // });

  //       }
  //     }
  //   ]
  // });
  // prompt.present();
  //}



  // signup(): void {

  

  //   var headers = new Headers();
  //   headers.set('Content-type', 'application/x-www-form-urlencoded');
  //   const uri = 'http://khelojiyo.com/api/users/register';
  //   this.http.post(uri, obj, { headers })
  //     .subscribe(res => {

  //       alert(res['_body']);

  //     }, (err) => {
  //       console.log(err);
  //     });

  // }

  // saveData(){
  //            var obj : User;
  //            obj = new User();
  //            if(this.flag)
  //             {
  //               obj.Email= this.u_email.toLowerCase();
  //               obj.LoginType= "gmail";
  //               obj.Password= "null";
  //             }
  //              else
  //             {
  //               obj.Email= this.Email.toLowerCase();
  //               obj.LoginType= "email";
  //               obj.Password= this.password;

  //             }
  //             obj.Name= this.firstname;
  //             obj.Username= this.lastname;

  //             this.firebaseProvider.addUser(obj).then(
  //               () => {
  //                 if(!this.flag)
  //                 {
  //                   alert("Please varifiy your email address.Check your mailbox for that.");
  //                   this.navCtrl.setRoot(LoginPage);
  //                 }
  //                 else{
  //                   this.navCtrl.setRoot(HomePage);
  //                 }
  //               },
  //               error => {
  //                 alert(error.message);
  //               }
  //             );
  //              //console.log("DataSaved:"+newSongRef.key+"--"+obj);
  //             // newSongRef.set({
  //             //   id: newSongRef.key,
  //             //   title: data.title
  //             // });

  // }


  selectcoach(value: boolean) {
    this.isCoach = value;
  }
  valid() {
    if (this.Firstname.length !== 0)
      return false;
    if (this.Lastname.length !== 0)
      return false;
    if (this.Email.length !== 0)
      return false;
    if (this.Phone.length !== 0)
      return false;
    if (this.Password.length !== 0)
      return false;
    if (this.Password_2.length !== 0)
      return false;
    if (this.Password !== this.Password_2)
      return false;
    else
      return true;
  }

  finish(){
    this.navCtrl.pop();
  }

}
