export class User {
    key?: string;
    Email: string;
    Name: string;
    Password: string;
    Username: string;
    LoginType: string;
    ////////////////////
    firstName: String;
    lastName: String;
    roleId: String;
    id: String;
    userName: String;
    normalizedUserName : String;
    email : String;
    normalizedEmail: String;
    emailConfirmed: boolean;
    passwordHash: String;
    securityStamp: String;
    concurrencyStamp: String;
    phoneNumber : String;
    phoneNumberConfirmed : boolean;
    twoFactorEnabled : boolean;
    accessFailedCount :String;

}
