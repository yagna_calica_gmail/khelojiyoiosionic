import { Error } from "./error.model";

export class APIResponse {
    status: string;
    errors: Error[];
    data: string;

    constructor(values: Object = {}) {

        Object.assign(this, values);

    }
}



