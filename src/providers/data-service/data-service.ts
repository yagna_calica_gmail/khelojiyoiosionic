import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';

import 'rxjs/add/operator/map';
import { User } from '../../model/user.model';

/*
  Generated class for the DataServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
export class Url {
 public ok="200";
 public user: User;

 public host = "http://192.168.10.135";
 public baseUrl = this.host+"/api/account/";
 public login =  this.baseUrl+"login";
 public register =  this.baseUrl+"register";
}


@Injectable()
export class DataServiceProvider {

  constructor(public http: HttpClient) {
    console.log('Hello DataServiceProvider Provider');
  }

  postRequset(url: string, param: any):any {
     //var headers = new Headers();
    //  headers.set('Content-type','application/json');
    // return this.http.post(url,param).map((response:any)=>response.json())

    return this.http.post(url, param, {headers  : {'Content-Type': 'application/json'} });

    

    // return this.http.get('assets/data/products.json')
    //   .map((response:Response)=>response.json());
    // var headers = new Headers();
    //    headers.set('Content-type','application/json');
    //   this.http.post(url ,param,{headers})
    //   .subscribe(res => {          
    //               return res['_body'];
    //   }, (err) => {
    //               return  err;
    //   });
 
  }

}
