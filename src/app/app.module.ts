import { GoogleauthPage } from '../pages/googleauth/googleauth';
// import { HttpClient } from '@angular/common/http';
import { TeamProfile } from '../pages/teamprofile/teamprofile';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { KheloJiyo } from './app.component';
import { HomePage } from '../pages/home/home';
import { ContactPage } from '../pages/contact/contact';
import { AboutPage } from '../pages/about/about';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/Register/register';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { ModelPage } from '../pages/Model/model';
import { MapslocationPage } from '../pages/mapslocation/mapslocation';
import { Geolocation } from '@ionic-native/geolocation';
import { Device } from '@ionic-native/device';
import { Facebook } from '@ionic-native/facebook';
import { FacebookloginPage } from '../pages/facebooklogin/facebooklogin';
import { SpinnerloaderPage } from '../pages/spinnerloader/spinnerloader';
import { DataServiceProvider, Url } from '../providers/data-service/data-service';
import { SpeechtotextPage } from '../pages/speechtotext/speechtotext';
import { SpeechRecognition } from '@ionic-native/speech-recognition';
import { TextToSpeech } from '@ionic-native/text-to-speech';
import { CameraPage } from '../pages/camera/camera';
import { Camera } from '@ionic-native/camera';
import { PhotoLibrary } from '@ionic-native/photo-library';
import { KeysPipe } from '../pipes/keys.pipe';
import { ListPage } from '../pages/list/list';
// Import the AF2 Module
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { FirebaseProvider } from '../providers/firebase/firebase';

import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { ForgotPage } from '../pages/forgot/forgot';
import { GooglePlus } from '@ionic-native/google-plus';
import { SplashScreen } from '@ionic-native/splash-screen';



//import { AlertController } from 'ionic-angular';
// AF2 Settings
export const firebaseConfig = {
  apiKey: "AIzaSyD6QoZtgVsMJnOlV2-5BS7R_OSJyTIeKr8",
  authDomain: "khelojiyoionic.firebaseapp.com",
  databaseURL: "https://khelojiyoionic.firebaseio.com",
  projectId: "khelojiyoionic",
  storageBucket: "khelojiyoionic.appspot.com",
  messagingSenderId: "899015697986"
};

@NgModule({
  declarations: [
    KheloJiyo,
    HomePage,
    ListPage,TabsPage,AboutPage,ContactPage,TeamProfile,LoginPage,RegisterPage,ModelPage,GoogleauthPage,MapslocationPage,FacebookloginPage,
    SpinnerloaderPage,SpeechtotextPage,CameraPage,KeysPipe,ForgotPage,

  ],
  imports: [
    BrowserModule,
    HttpModule,HttpClientModule,
    IonicModule.forRoot(KheloJiyo),
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    KheloJiyo,
    HomePage,
    ListPage,TabsPage,AboutPage,ContactPage,TeamProfile,LoginPage,RegisterPage,ModelPage,GoogleauthPage,MapslocationPage,FacebookloginPage,
    SpinnerloaderPage,SpeechtotextPage,
    CameraPage,ForgotPage
  ],
  providers: [
    StatusBar,
    SplashScreen,TabsPage,AboutPage,ContactPage,TeamProfile,LoginPage,RegisterPage,ModelPage,GooglePlus,MapslocationPage,FacebookloginPage,
    DataServiceProvider,SpeechRecognition,TextToSpeech,
    {provide: ErrorHandler, useClass: IonicErrorHandler},Geolocation,
    Device, Facebook,Camera,PhotoLibrary,CameraPage,ForgotPage,
    FirebaseProvider,Url,
    AngularFireAuth
  ],
})
export class AppModule {}

