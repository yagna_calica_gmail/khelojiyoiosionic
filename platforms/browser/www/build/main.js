webpackJsonp([2],{

/***/ 161:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AboutPage = /** @class */ (function () {
    function AboutPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    AboutPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-about',template:/*ion-inline-start:"E:\Ionic\Ionic-master\src\pages\about\about.html"*/'<ion-header padding>\n  <ion-title>\n    About\n  </ion-title>\n</ion-header>\n\n<ion-content padding>\n   <p>\n    If you get lost, the <a href="http://ionicframework.com/docs/v2">docs</a> will be your guide.\n  </p>\n\n\n</ion-content>\n'/*ion-inline-end:"E:\Ionic\Ionic-master\src\pages\about\about.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]])
    ], AboutPage);
    return AboutPage;
}());

//# sourceMappingURL=about.js.map

/***/ }),

/***/ 162:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ContactPage = /** @class */ (function () {
    function ContactPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    ContactPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-contact',template:/*ion-inline-start:"E:\Ionic\Ionic-master\src\pages\contact\contact.html"*/'<ion-header>\n\n  <ion-title>\n\n    Contact\n\n  </ion-title>\n\n</ion-header>\n\n<ion-content>\n\n  <ion-list>\n\n    <ion-list-header>Follow us on Twitter</ion-list-header>\n\n    <ion-item>\n\n      <ion-icon name="ionic" item-start></ion-icon>\n\n      geoduck\n\n    </ion-item>\n\n  </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"E:\Ionic\Ionic-master\src\pages\contact\contact.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]])
    ], ContactPage);
    return ContactPage;
}());

//# sourceMappingURL=contact.js.map

/***/ }),

/***/ 202:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 202;

/***/ }),

/***/ 203:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BledetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_ble__ = __webpack_require__(159);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var BledetailPage = /** @class */ (function () {
    function BledetailPage(navCtrl, navParams, ble, toastCtrl, ngZone) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.ble = ble;
        this.toastCtrl = toastCtrl;
        this.ngZone = ngZone;
        this.peripheral = {};
        var device = navParams.get('device');
        this.setStatus('Connecting to ' + device.name || device.id);
        this.ble.connect(device.id).subscribe(function (peripheral) { return _this.onConnected(peripheral); }, function (peripheral) { return _this.onDeviceDisconnected(peripheral); });
    }
    BledetailPage.prototype.onConnected = function (peripheral) {
        var _this = this;
        this.ngZone.run(function () {
            _this.peripheral = peripheral;
            _this.ble.startNotification(_this.peripheral.id, '0003cdd0-0000-1000-8000-00805f9b0131', '0003cdd1-0000-1000-8000-00805f9b0131').subscribe(function (notificationData) {
                var result = new Uint8Array(notificationData).buffer;
                var resulthex = Array.prototype.map.call(new Uint8Array(result), function (x) { return ('00' + x.toString(16)).slice(-2); }).join('');
                console.log("Notification:" + resulthex);
                alert(resulthex);
            }, function (error) {
                console.log("Error Notification" + JSON.stringify(error));
            });
            var toast = _this.toastCtrl.create({
                message: peripheral.services,
                duration: 3000,
                position: 'middle'
            });
            toast.present();
            console.log('Services ' + JSON.stringify(peripheral.services));
        });
        // let characteristic=peripheral.services.characteristic[5]
        // this.ble.startNotification(this.peripheral.id,'cdd0','cdd1')
        // Update the UI with the current state
    };
    BledetailPage.prototype.onDeviceDisconnected = function (peripheral) {
        var toast = this.toastCtrl.create({
            message: 'The peripheral unexpectedly disconnected',
            duration: 3000,
            position: 'middle'
        });
        toast.present();
    };
    // Disconnect peripheral when leaving the page
    BledetailPage.prototype.ionViewWillLeave = function () {
        var _this = this;
        console.log('ionViewWillLeave disconnecting Bluetooth');
        this.ble.disconnect(this.peripheral.id).then(function () { return console.log('Disconnected ' + JSON.stringify(_this.peripheral)); }, function () { return console.log('ERROR disconnecting ' + JSON.stringify(_this.peripheral)); });
    };
    BledetailPage.prototype.setStatus = function (message) {
        var _this = this;
        console.log(message);
        this.ngZone.run(function () {
            _this.statusMessage = message;
        });
    };
    //  buf2hex(buffer) { // buffer is an ArrayBuffer
    //     return Array.prototype.map.call(new Uint8Array(buffer), x => ('00' + x.toString(16)).slice(-2)).join('');
    //   }
    BledetailPage.prototype.sendcommand = function () {
        var value = "6802026843024516";
        var buffer = this.stringToBytes(value);
        console.log('command ' + value + "buffer " + buffer);
        var toast1 = this.toastCtrl.create({
            message: "write command" + value,
            duration: 1000,
            position: 'middle'
        });
        toast1.present();
        // this.ble.read(this.peripheral.id, '0003cdd0-0000-1000-8000-00805f9b0131', '0003cdd1-0000-1000-8000-00805f9b0131').then(function (data){
        //   //console.log("READ:" + String.fromCharCode.apply(null, new Uint8Array(data)));
        //   console.log ("Read" + JSON.stringify(data));
        // }, function(error) {
        //   console.log("Error Read" + JSON.stringify(error));
        // });
        // this.ble.read(this.peripheral.id, '0003cdd0-0000-1000-8000-00805f9b0131', '0003cdd1-0000-1000-8000-00805f9b0131').then(function (data){
        //   //console.log("READ:" + String.fromCharCode.apply(null, new Uint8Array(data)));
        //   console.log ("Read" + JSON.stringify(data));
        // }, function(error) {
        //   console.log("Error Read" + JSON.stringify(error));
        // });
        // for(let i=0;i<buffer.byteLength;i++)
        // {
        //   console.log( "buffer "+ buffer.slice[i]);
        // }
        this.ble.writeWithoutResponse(this.peripheral.id, "0003cdd0-0000-1000-8000-00805f9b0131", "0003cdd2-0000-1000-8000-00805f9b0131", buffer).then((function (data) {
            //console.log("READ:" + String.fromCharCode.apply(null, new Uint8Array(data)));
            console.log("write" + JSON.stringify(data));
        }), function (error) {
            console.log("Error  " + JSON.stringify(error));
        });
    };
    //    stringToBytes(string) {
    //     var array = new Uint8Array(string.length);
    //     for (var i = 0, l = string.length; i < l; i++) {
    //         array[i] = string.charCodeAt(i);
    //         console.log( "buffer "+ array[i] );
    //      }
    //      return array.buffer;
    //  }
    BledetailPage.prototype.stringToBytes = function (string) {
        var array = new Int8Array(string.length / 2);
        for (var i = 0; i < string.length; i++) {
            if (i % 2 == 0) {
                var subsrting = string.substr(i, 2);
                console.log("substring " + subsrting);
                array[i / 2] = parseInt(subsrting, 16);
                console.log("buffer " + array[i]);
            }
        }
        // array[0]=104;
        // array[1]=2;
        // array[2]=2;
        // array[3]=104;
        // array[4]=67;
        // array[5]=2;
        // array[6]=69;
        // array[7]=22;
        console.log("buffer " + array);
        // console.log( "buffer "+ new Uint8Array(array));
        return array.buffer;
    };
    BledetailPage.prototype.bytesToString = function (buffer) {
        return String.fromCharCode.apply(null, new Uint8Array(buffer));
    };
    BledetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-bledetail',template:/*ion-inline-start:"E:\Ionic\Ionic-master\src\pages\bledetail\bledetail.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>{{ peripheral.name || \'Device\' }}</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="padding">\n\n\n\n  <ion-card>\n\n    <ion-card-header>\n\n      {{ peripheral.name || \'Unnamed\' }}\n\n    </ion-card-header>\n\n    <ion-card-content>\n\n      {{ peripheral.id }}\n\n    </ion-card-content>\n\n  </ion-card>\n\n\n\n  <ion-card>\n\n    <ion-card-header>\n\n      Services\n\n    </ion-card-header>\n\n\n\n    <ion-list>\n\n      <ion-item *ngFor="let service of peripheral.services">\n\n        {{service}}\n\n      </ion-item>\n\n    </ion-list>\n\n  </ion-card>\n\n\n\n\n\n  <ion-card>\n\n    <button ion-button (click)="sendcommand()">button</button>\n\n  </ion-card>\n\n\n\n  <ion-card>\n\n    <ion-card-header>\n\n      Details\n\n    </ion-card-header>\n\n    <ion-card-content>\n\n      <pre>{{peripheral | json }}</pre>\n\n    </ion-card-content>\n\n  </ion-card>\n\n</ion-content>\n\n\n\n<ion-footer>\n\n  <ion-toolbar>\n\n    <p>{{ statusMessage }}</p>\n\n  </ion-toolbar>\n\n</ion-footer>\n\n'/*ion-inline-end:"E:\Ionic\Ionic-master\src\pages\bledetail\bledetail.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_ble__["a" /* BLE */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */]])
    ], BledetailPage);
    return BledetailPage;
}());

//# sourceMappingURL=bledetail.js.map

/***/ }),

/***/ 246:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/hometest/hometest.module": [
		537,
		1
	],
	"../pages/testpage/testpage.module": [
		538,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 246;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 287:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BlehomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_native_ble__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__bledetail_bledetail__ = __webpack_require__(203);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var BlehomePage = /** @class */ (function () {
    function BlehomePage(navCtrl, toastCtrl, ble, ngZone) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.ble = ble;
        this.ngZone = ngZone;
        this.devices = [];
        this.ble.startStateNotifications().subscribe(function (state) {
            console.log("Bluetooth is " + state);
            if (state == "on") {
                _this.scan();
            }
            else if (state == "off") {
                alert("off");
                _this.ngZone.run(function () {
                    _this.devices = [];
                });
            }
        });
    }
    BlehomePage.prototype.ionViewDidEnter = function () {
        if (!this.ble.enable()) {
            this.ble.showBluetoothSettings();
        }
        else {
            this.scan();
        }
    };
    BlehomePage.prototype.scan = function () {
        var _this = this;
        if (!this.ble.enable()) {
            this.ble.showBluetoothSettings();
        }
        else {
            this.setStatus('Scanning for Bluetooth LE Devices');
            this.devices = []; // clear list
            this.ble.scan([], 5).subscribe(function (device) { return _this.onDeviceDiscovered(device); }, function (error) { return _this.scanError(error); });
            setTimeout(this.setStatus.bind(this), 5000, 'Scan complete');
        }
    };
    BlehomePage.prototype.onDeviceDiscovered = function (device) {
        var _this = this;
        console.log('Discovered ' + JSON.stringify(device, null, 2));
        this.ngZone.run(function () {
            _this.devices.push(device);
        });
    };
    // If location permission is denied, you'll end up here
    BlehomePage.prototype.scanError = function (error) {
        this.setStatus('Error ' + error);
        var toast = this.toastCtrl.create({
            message: 'Error scanning for Bluetooth low energy devices',
            position: 'middle',
            duration: 5000
        });
        toast.present();
    };
    BlehomePage.prototype.setStatus = function (message) {
        var _this = this;
        console.log(message);
        this.ngZone.run(function () {
            _this.statusMessage = message;
        });
    };
    BlehomePage.prototype.deviceSelected = function (device) {
        console.log(JSON.stringify(device) + ' selected');
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__bledetail_bledetail__["a" /* BledetailPage */], {
            device: device
        });
    };
    BlehomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-blehome',template:/*ion-inline-start:"E:\Ionic\Ionic-master\src\pages\blehome\blehome.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Ionic-Master</ion-title>\n    <ion-buttons end>\n      <button ion-button tappable>\n        <ion-icon name="notifications"></ion-icon>\n      </button>\n      <button ion-button tappable>\n        <ion-icon name="cog"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n    <ion-title>\n        BLE Connect\n      </ion-title>\n      <ion-buttons end>\n        <button ion-button (click)="scan()">\n          Scan\n        </button>\n      </ion-buttons>\n   <ion-list>\n    <button ion-item *ngFor="let device of devices" (click)="deviceSelected(device)">\n      <h2>{{ device.name || \'Unnamed\' }}</h2>\n      <p>{{ device.id }}</p>\n      <p>RSSI: {{ device.rssi }}</p>\n    </button>\n   </ion-list>\n</ion-content>\n\n<ion-footer>\n  <ion-toolbar>\n    <p>{{ statusMessage }}</p>\n  </ion-toolbar>\n</ion-footer>\n'/*ion-inline-end:"E:\Ionic\Ionic-master\src\pages\blehome\blehome.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_0__ionic_native_ble__["a" /* BLE */],
            __WEBPACK_IMPORTED_MODULE_1__angular_core__["M" /* NgZone */]])
    ], BlehomePage);
    return BlehomePage;
}());

//# sourceMappingURL=blehome.js.map

/***/ }),

/***/ 288:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GoogleauthPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_plus__ = __webpack_require__(289);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the GoogleauthPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var GoogleauthPage = /** @class */ (function () {
    function GoogleauthPage(navCtrl, googlePlus) {
        this.navCtrl = navCtrl;
        this.googlePlus = googlePlus;
        this.isLoggedIn = false;
    }
    GoogleauthPage.prototype.login = function () {
        var _this = this;
        this.googlePlus.login({})
            .then(function (res) {
            console.log(res);
            alert(res);
            _this.displayName = res.displayName;
            _this.email = res.email;
            _this.familyName = res.familyName;
            _this.givenName = res.givenName;
            _this.userId = res.userId;
            _this.imageUrl = res.imageUrl;
            _this.isLoggedIn = true;
        })
            .catch(function (err) { return alert(err); });
    };
    GoogleauthPage.prototype.logout = function () {
        var _this = this;
        this.googlePlus.logout()
            .then(function (res) {
            console.log(res);
            _this.displayName = "";
            _this.email = "";
            _this.familyName = "";
            _this.givenName = "";
            _this.userId = "";
            _this.imageUrl = "";
            _this.isLoggedIn = false;
        })
            .catch(function (err) { return console.error(err); });
    };
    GoogleauthPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-googleauth',template:/*ion-inline-start:"E:\Ionic\Ionic-master\src\pages\googleauth\googleauth.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Ionic-Master</ion-title>\n    <ion-buttons end>\n      <button ion-button tappable>\n        <ion-icon name="notifications"></ion-icon>\n      </button>\n      <button ion-button tappable>\n        <ion-icon name="cog"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <div *ngIf="isLoggedIn; else loginTemplate">\n    <h1>Welcome, {{displayName}}!</h1>\n    <p>Email: {{email}}<br>\n      Family Name: {{familyName}}<br>\n      Given Name: {{givenName}}<br>\n      User ID: {{userId}}</p>\n    <p><ion-avatar item-left>\n        <img src="{{imageUrl}}">\n       </ion-avatar></p>\n    <p><button ion-button (click)="logout()">Logout From Google</button></p>\n  </div>\n\n  <ng-template #loginTemplate>\n    <h1>Please Login to see your Google Account Information</h1>\n    <p><button ion-button (click)="login()">Login With Google</button></p>\n  </ng-template>\n</ion-content>\n'/*ion-inline-end:"E:\Ionic\Ionic-master\src\pages\googleauth\googleauth.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__ionic_native_google_plus__["a" /* GooglePlus */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_plus__["a" /* GooglePlus */]])
    ], GoogleauthPage);
    return GoogleauthPage;
}());

//# sourceMappingURL=googleauth.js.map

/***/ }),

/***/ 290:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__about_about__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__contact_contact__ = __webpack_require__(162);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var TabPage = /** @class */ (function () {
    function TabPage(platform, statusBar, splashScreen) {
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_4__about_about__["a" /* AboutPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_5__contact_contact__["a" /* ContactPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    TabPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"E:\Ionic\Ionic-master\src\pages\tabpage\tabpage.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Ionic-Master</ion-title>\n    <ion-buttons end>\n      <button ion-button tappable>\n        <ion-icon name="notifications"></ion-icon>\n      </button>\n      <button ion-button tappable>\n        <ion-icon name="cog"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content padding>\n  <!-- <h3>Ionic Menu Starter</h3>\n  <p>\n    If you get lost, the <a href="http://ionicframework.com/docs/v2">docs</a> will show you the way.\n  </p>\n  <button ion-button secondary menuToggle>Toggle Menu</button> -->\n  <ion-tabs>\n    <ion-tab [root]="tab2Root" tabTitle="About" tabIcon="information-circle"></ion-tab>\n    <ion-tab [root]="tab3Root" tabTitle="Contact" tabIcon="contacts"></ion-tab>\n  </ion-tabs>\n\n</ion-content>\n'/*ion-inline-end:"E:\Ionic\Ionic-master\src\pages\tabpage\tabpage.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], TabPage);
    return TabPage;
}());

//# sourceMappingURL=tabpage.js.map

/***/ }),

/***/ 304:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ForgotPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__login_login__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Register_register__ = __webpack_require__(97);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_firebase_firebase__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__home_home__ = __webpack_require__(71);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ForgotPage = /** @class */ (function () {
    function ForgotPage(navCtrl, http, afDatabase, firebaseProvider) {
        this.navCtrl = navCtrl;
        this.http = http;
        this.afDatabase = afDatabase;
        this.firebaseProvider = firebaseProvider;
        this.apiUrl = 'http://72.249.170.12/BluetoothApi/api/Login/Login';
        this.passwordType = 'password';
        this.paIcon = 'eye-off';
        debugger;
        this.user = this.firebaseProvider.getShoppingItems();
    }
    //https://www.djamware.com/post/59924f9080aca768e4d2b12e/ionic-3-consuming-rest-api-using-new-angular-43-httpclient
    ForgotPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        setTimeout(function () {
            _this.email.setFocus();
        }, 500);
    };
    // login(): void {
    //   const obj = {
    //     "UserName":"admin",
    //     "Password":"admin"
    //    }
    // var headers = new Headers();
    //     headers.set('Content-type','application/json');
    //       console.log('doing'+ JSON.stringify(obj));
    //       debugger;
    //        const uri = 'http://72.249.170.12/HRDCServiceOnline/user.svc/UserLogin';
    //        this.http.post(uri,JSON.stringify(obj),{headers})
    //       .subscribe(res => {
    //         console.log("done");
    //         alert('done');
    //       }, (err) => {
    //         console.log(err);
    //       });
    // }
    ForgotPage.prototype.login = function () {
        var _this = this;
        if (!this.username) {
            return;
        }
        this.firebaseProvider.resetPassword(this.username)
            .then(function () {
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_0__login_login__["a" /* LoginPage */]);
            alert("You reset password requset will be sent to your Email, Please chceck your inbox now!");
        }, function (error) { alert(error.message); _this.loginError = error.message; });
    };
    ForgotPage.prototype.signup = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_1__Register_register__["a" /* RegisterPage */], { "email": "null" });
    };
    ForgotPage.prototype.hideShowPassword = function () {
        this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
        this.paIcon = this.paIcon === 'eye-off' ? 'eye' : 'eye-off';
    };
    ForgotPage.prototype.loginWithGoogle = function () {
        var _this = this;
        debugger;
        this.firebaseProvider.signInWithGoogle()
            .then(function (user) {
            if (user.user.emailVerified) {
                _this.firebaseProvider.Superuser = user.user;
                var pre_user = user.user;
                if (pre_user) {
                    _this.firebaseProvider.findEmail(pre_user.email).then(function (ref) {
                        _this.uber = ref;
                        ////
                        _this.uber.subscribe(function (res) {
                            console.log(res);
                            var m_user = res;
                            if (m_user.length === 0) {
                                _this.firebaseProvider.isRegistredUser = false;
                                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_1__Register_register__["a" /* RegisterPage */], { "email": pre_user.email });
                            }
                            else {
                                _this.firebaseProvider.username = res[0].Username;
                                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__home_home__["a" /* HomePage */]);
                            }
                        }, function () {
                            _this.firebaseProvider.isRegistredUser = false;
                            return false;
                        });
                    });
                }
            }
            else {
                alert("Email not varifired.Please check your mailbox");
            }
        }, function (error) { return console.log(error.message); });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["_8" /* ViewChild */])('email'),
        __metadata("design:type", Object)
    ], ForgotPage.prototype, "email", void 0);
    ForgotPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["m" /* Component */])({
            selector: 'page-forgot',template:/*ion-inline-start:"E:\Ionic\Ionic-master\src\pages\forgot\forgot.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Ionic-Master</ion-title>\n    <ion-buttons end>\n      <button ion-button tappable>\n        <ion-icon name="notifications"></ion-icon>\n      </button>\n      <button ion-button tappable>\n        <ion-icon name="cog"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content padding class="login auth-page">\n    <div class="login-content">\n      <!-- Logo -->\n    <div padding-horizontal text-center class="animated fadeInDown">\n        <br/>\n        <h2 ion-text class="text-primary">\n          <strong> Forgot Password ?</strong>\n        </h2>\n      </div>\n  <ion-row>\n  </ion-row>\n  <div >\n    <form #loginForm="ngForm"  autocomplete="off">\n      <ion-row>\n        <ion-col>\n          <ion-list inset>\n            <ion-item>\n                <ion-label floating>\n                    <ion-icon name="mail"\n                     item-start\n                     class="text-primary">\n                    </ion-icon>\n                    Email\n                  </ion-label>\n              <ion-input\n               name="username"\n                id="loginField"\n                type="email"\n                 required\n                  [(ngModel)]=\'username\'\n                 #email></ion-input>\n            </ion-item>\n\n          </ion-list>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <div *ngIf="error" class="alert alert-danger">{{ error }}</div>\n          <button ion-button class="submit-btn" full type="submit" (click)="login()"\n                  [disabled]="!loginForm.form.valid">Submit\n          </button>\n        </ion-col>\n      </ion-row>\n      <br/>\n      <B><div style="text-align:center">If you\'re a new user, please sign up.</div></B>\n      <ion-row class="right-text">\n          <ion-list  class="right-text">\n\n          <button  ion-button icon-left block clear (click)="loginWithGoogle()">\n            <ion-icon name="logo-google"></ion-icon>\n            Log in with Google\n          </button>\n\n          <button ion-button icon-left block clear (click)="signup()">\n            <ion-icon name="person-add"></ion-icon>\n            Sign up with Email\n          </button>\n        </ion-list>\n      </ion-row>\n\n\n    </form>\n    See Here:\n    <div><ul *ngFor="let profile of uber | async">\n      <li> {{ profile.Email}}:{{ profile.Name}} </li>\n   </ul></div>\n  </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"E:\Ionic\Ionic-master\src\pages\forgot\forgot.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_5__providers_firebase_firebase__["a" /* FirebaseProvider */]])
    ], ForgotPage);
    return ForgotPage;
}());

//# sourceMappingURL=forgot.js.map

/***/ }),

/***/ 305:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MapslocationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__ = __webpack_require__(306);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_device__ = __webpack_require__(307);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the MapslocationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MapslocationPage = /** @class */ (function () {
    function MapslocationPage(navCtrl, platform, geolocation, device) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.geolocation = geolocation;
        this.device = device;
        this.markers = [];
        debugger;
        platform.ready().then(function () {
            debugger;
            _this.initMap();
        });
    }
    MapslocationPage.prototype.initMap = function () {
        var _this = this;
        this.geolocation.getCurrentPosition().then(function (resp) {
            var mylocation = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
            debugger;
            _this.map = new google.maps.Map(_this.mapElement.nativeElement, {
                zoom: 18,
                center: mylocation
            });
        });
        var watch = this.geolocation.watchPosition();
        watch.subscribe(function (data) {
            _this.deleteMarkers();
            _this.updateGeolocation(_this.device.uuid, data.coords.latitude, data.coords.longitude);
            var updatelocation = new google.maps.LatLng(data.coords.latitude, data.coords.longitude);
            var image = 'assets/imgs/pin.png';
            _this.addMarker("XDuce", updatelocation, image);
            var latLng2 = new google.maps.LatLng(23.033048, 72.562212);
            _this.addMarker("Municipal Market", latLng2, image);
            _this.setMapOnAll(_this.map);
        });
    };
    MapslocationPage.prototype.addMarker = function (name, location, image) {
        var marker = new google.maps.Marker({
            title: name,
            position: location,
            map: this.map,
            icon: image
        });
        this.markers.push(marker);
        this.addInfoWindowToMarker(marker);
    };
    MapslocationPage.prototype.addInfoWindowToMarker = function (marker) {
        var _this = this;
        var infoWindowContent = '<div id="content"><h1 id="firstHeading" class="firstHeading">' + marker.title + '</h1></div>';
        var infoWindow = new google.maps.InfoWindow({
            content: infoWindowContent
        });
        marker.addListener('click', function () {
            infoWindow.open(_this.map, marker);
        });
    };
    MapslocationPage.prototype.setMapOnAll = function (map) {
        for (var i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(map);
        }
    };
    MapslocationPage.prototype.clearMarkers = function () {
        this.setMapOnAll(null);
    };
    MapslocationPage.prototype.deleteMarkers = function () {
        this.clearMarkers();
        this.markers = [];
    };
    MapslocationPage.prototype.updateGeolocation = function (uuid, lat, lng) {
    };
    MapslocationPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MapslocationPage');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], MapslocationPage.prototype, "mapElement", void 0);
    MapslocationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-mapslocation',template:/*ion-inline-start:"E:\Ionic\Ionic-master\src\pages\mapslocation\mapslocation.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Ionic-Master</ion-title>\n    <ion-buttons end>\n      <button ion-button tappable>\n        <ion-icon name="notifications"></ion-icon>\n      </button>\n      <button ion-button tappable>\n        <ion-icon name="cog"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <div #map id="map"></div>\n</ion-content>\n'/*ion-inline-end:"E:\Ionic\Ionic-master\src\pages\mapslocation\mapslocation.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_device__["a" /* Device */]])
    ], MapslocationPage);
    return MapslocationPage;
}());

//# sourceMappingURL=mapslocation.js.map

/***/ }),

/***/ 308:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Model_model__ = __webpack_require__(309);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(310);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_debounceTime__ = __webpack_require__(528);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_debounceTime___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_debounceTime__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__ = __webpack_require__(166);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_http__ = __webpack_require__(55);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ListPage = /** @class */ (function () {
    function ListPage(navCtrl, navParams, httpClient, modalCtrl, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.httpClient = httpClient;
        this.modalCtrl = modalCtrl;
        this.http = http;
        this.pageno = 0;
        this.items = [];
        var obj = { UserId: 1, ParentId: 1, Page: 0, PageSize: 10 };
        this.searchControl = new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormControl */]();
        this.apipaggingcall(this.pageno);
        // this.films = this.httpClient.get('https://swapi.co/api/films');
        //  var headers = new Headers();
        //     headers.set('Content-type','application/json');
        //       console.log('doing'+ JSON.stringify(obj));
        //       debugger;
        //        const uri = 'http://72.249.170.12/InsuranceCalicaApi/api/Quotation/GetQuotationListByTLTC';
        //        this.http.post(uri,JSON.stringify(obj),{headers})
        //  // this.films
        //   .subscribe(data => {
        //     debugger;
        //     var json=data;
        //     console.log('my data: ', json);
        //     alert(data.json()['ResponseData'][0].QuotationId);
        //   //   this.Tags=JSON.stringify(data);
        //   //  // console.log('my data: ', this.Tags);
        //   //  var datanew =JSON.parse(this.Tags);
        //   this.items = [];
        //   for (let item of data.json()['ResponseData'])
        //   {
        //     console.log("title",item.QuotationCode);
        //     this.items.push({
        //           title: 'Item ' + item.QuotationCode,
        //           note: 'This is item #' + item.QuotationId
        //         });
        //   }
        //   this.itemtemp =this.items
        //   })
    }
    ListPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.searchControl.valueChanges.debounceTime(100).subscribe(function (search) {
            _this.items = _this.itemtemp;
            _this.setFilteredItems();
        });
    };
    ListPage.prototype.itemTapped = function (event, item) {
        // That's right, we're pushing to ourselves!
        // this.navCtrl.push(ListPage, {
        //   item: item
        // });
        var index = this.items.indexOf(item);
        alert(index);
        this.items.splice(index, 1);
    };
    ListPage.prototype.itemTappededit = function (event, item) {
        // That's right, we're pushing to ourselves!
        // this.navCtrl.push(ListPage, {
        //   item: item
        // });
        var index = this.items.indexOf(item);
        alert(index);
        this.items.splice(index, 1, {
            title: 'Item Dummy',
            note: 'This is item # dummy'
        });
    };
    ListPage.prototype.openModal = function (event, item) {
        var _this = this;
        this.index = this.items.indexOf(item);
        var data = { message: item };
        var modalPage = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_0__Model_model__["a" /* ModelPage */], data);
        modalPage.onDidDismiss(function (data) {
            console.log(data);
            _this.items.splice(_this.index, 1, {
                title: data.title,
                note: data.note
            });
        });
        modalPage.present();
    };
    ListPage.prototype.setFilteredItems = function () {
        var _this = this;
        this.items = this.items.filter(function (item) {
            return item.title.toLowerCase().indexOf(_this.searchTerm.toLowerCase()) > -1;
        });
    };
    ListPage.prototype.doInfinite = function (infiniteScroll) {
        if (this.lastcallpageno != this.pageno) {
            this.apipaggingcall(this.pageno);
            infiniteScroll.complete();
        }
        else {
            infiniteScroll.complete();
            this.ionViewDidLoad();
        }
    };
    ListPage.prototype.apipaggingcall = function (pageno) {
        var _this = this;
        this.lastcallpageno = pageno;
        var obj = { UserId: 1, ParentId: 1, Page: pageno, PageSize: 10 };
        this.searchControl = new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormControl */]();
        // this.films = this.httpClient.get('https://swapi.co/api/films');
        var headers = new __WEBPACK_IMPORTED_MODULE_7__angular_http__["a" /* Headers */]();
        headers.set('Content-type', 'application/json');
        console.log('doing' + JSON.stringify(obj));
        debugger;
        var uri = 'http://72.249.170.12/InsuranceCalicaApi/api/Quotation/GetQuotationListByTLTC';
        this.http.post(uri, JSON.stringify(obj), { headers: headers })
            .subscribe(function (data) {
            debugger;
            var json = data;
            console.log('my data: ', json);
            //   this.Tags=JSON.stringify(data);
            //  // console.log('my data: ', this.Tags);
            //  var datanew =JSON.parse(this.Tags);
            if (data.json()['ResponseData'].length > 0) {
                for (var _i = 0, _a = data.json()['ResponseData']; _i < _a.length; _i++) {
                    var item = _a[_i];
                    console.log("title", item.QuotationCode);
                    _this.items.push({
                        title: 'Item ' + item.QuotationCode,
                        note: 'This is item #' + item.QuotationId
                    });
                }
                _this.itemtemp = _this.items;
                _this.pageno++;
                alert('call');
            }
        });
    };
    ListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-list',template:/*ion-inline-start:"E:\Ionic\Ionic-master\src\pages\list\list.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Ionic-Master</ion-title>\n    <ion-buttons end>\n      <button ion-button tappable>\n        <ion-icon name="notifications"></ion-icon>\n      </button>\n      <button ion-button tappable>\n        <ion-icon name="cog"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n    <ion-searchbar [(ngModel)]="searchTerm" [formControl]="searchControl" padding></ion-searchbar>\n  <ion-list>\n    <button ion-item *ngFor="let item of items" >\n      <!-- <ion-icon [name]="item.icon" item-start></ion-icon> -->\n      {{item.title}}\n      <div class="item-note" item-end >\n       <!-- {{item.note}} -->\n       <ion-icon ios="ios-create" md="md-create" (click)="openModal($event, item)" padding></ion-icon>\n       <ion-icon ios="ios-trash" md="md-trash" (click)="itemTapped($event, item)"></ion-icon>\n      </div>\n    </button>\n  </ion-list>\n  <div *ngIf="selectedItem" padding>\n    You navigated here from <b>{{selectedItem.title}}</b>\n  </div>\n  <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n      <ion-infinite-scroll-content></ion-infinite-scroll-content>\n    </ion-infinite-scroll>\n</ion-content>\n'/*ion-inline-end:"E:\Ionic\Ionic-master\src\pages\list\list.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* ModalController */], __WEBPACK_IMPORTED_MODULE_7__angular_http__["b" /* Http */]])
    ], ListPage);
    return ListPage;
}());

//# sourceMappingURL=list.js.map

/***/ }),

/***/ 309:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModelPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ModelPage = /** @class */ (function () {
    function ModelPage(viewCtrl, navParams) {
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
    }
    ModelPage.prototype.ionViewDidLoad = function () {
        console.log(this.navParams.get('message'));
        this.title = this.navParams.get('message').title;
        this.note = this.navParams.get('message').note;
    };
    ModelPage.prototype.closeModal = function () {
        this.viewCtrl.dismiss({
            title: this.title,
            note: this.note
        });
    };
    ModelPage.prototype.updatesubmit = function () {
        this.viewCtrl.dismiss({
            title: this.title,
            note: this.note
        });
    };
    ModelPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-model',template:/*ion-inline-start:"E:\Ionic\Ionic-master\src\pages\Model\model.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>ModalPage</ion-title>\n\n    <ion-buttons end>\n\n    <button ion-button (click)="closeModal()">Close</button>\n\n    </ion-buttons>\n\n</ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n\n  <ion-row>\n\n  </ion-row>\n\n  <div >\n\n    <form #modelForm="ngForm" (ngSubmit)="updatesubmit()" autocomplete="off">\n\n      <ion-row>\n\n        <ion-col>\n\n          <ion-list inset>\n\n            <ion-item>\n\n              <ion-input placeholder="Title" name="title" id="titleField"\n\n                         type="text" required [(ngModel)]="title" ></ion-input>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-input placeholder="Note" name="note" id="noteField"\n\n                         type="text" required [(ngModel)]="note"></ion-input>\n\n            </ion-item>\n\n          </ion-list>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row>\n\n        <ion-col>\n\n          <button ion-button class="submit-btn" full type="submit"\n\n                  [disabled]="!modelForm.form.valid">Update\n\n          </button>\n\n        </ion-col>\n\n      </ion-row>\n\n    </form>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"E:\Ionic\Ionic-master\src\pages\Model\model.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["l" /* ViewController */], __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["i" /* NavParams */]])
    ], ModelPage);
    return ModelPage;
}());

//# sourceMappingURL=model.js.map

/***/ }),

/***/ 311:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FacebookloginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_facebook__ = __webpack_require__(312);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FacebookloginPage = /** @class */ (function () {
    function FacebookloginPage(fb) {
        var _this = this;
        this.fb = fb;
        this.isLoggedIn = false;
        fb.getLoginStatus()
            .then(function (res) {
            console.log(res.status);
            if (res.status === "connect") {
                _this.isLoggedIn = true;
            }
            else {
                _this.isLoggedIn = false;
            }
        })
            .catch(function (e) { return console.log(e); });
    }
    FacebookloginPage.prototype.login = function () {
        var _this = this;
        this.fb.login(['public_profile', 'user_friends', 'email'])
            .then(function (res) {
            if (res.status === "connected") {
                _this.isLoggedIn = true;
                _this.getUserDetail(res.authResponse.userID);
            }
            else {
                _this.isLoggedIn = false;
            }
        })
            .catch(function (e) { return console.log('Error logging into Facebook', e); });
    };
    FacebookloginPage.prototype.logout = function () {
        var _this = this;
        this.fb.logout()
            .then(function (res) { return _this.isLoggedIn = false; })
            .catch(function (e) { return console.log('Error logout from Facebook', e); });
    };
    FacebookloginPage.prototype.getUserDetail = function (userid) {
        var _this = this;
        this.fb.api("/" + userid + "/?fields=id,email,name,picture,gender", ["public_profile"])
            .then(function (res) {
            console.log(res);
            _this.users = res;
        })
            .catch(function (e) {
            console.log(e);
        });
    };
    FacebookloginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-facebooklogin',template:/*ion-inline-start:"E:\Ionic\Ionic-master\src\pages\facebooklogin\facebooklogin.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Ionic-Master</ion-title>\n    <ion-buttons end>\n      <button ion-button tappable>\n        <ion-icon name="notifications"></ion-icon>\n      </button>\n      <button ion-button tappable>\n        <ion-icon name="cog"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content padding>\n  <div *ngIf="isLoggedIn; else facebookLogin">\n    <h2>Hi, {{users.name}} ({{users.email}})</h2>\n    <p>\n      Gender: {{users.gender}}\n    </p>\n    <p>\n      <img src="{{users.picture.data.url}}" width="100" alt="{{users.name}}" />\n    </p>\n    <p>\n      <button ion-button icon-right (click)="logout()">\n        Logout\n      </button>\n    </p>\n  </div>\n  <ng-template #facebookLogin>\n    <button ion-button icon-right (click)="login()">\n      Login with\n      <ion-icon name="logo-facebook"></ion-icon>\n    </button>\n  </ng-template>\n</ion-content>\n'/*ion-inline-end:"E:\Ionic\Ionic-master\src\pages\facebooklogin\facebooklogin.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_facebook__["a" /* Facebook */]])
    ], FacebookloginPage);
    return FacebookloginPage;
}());

//# sourceMappingURL=facebooklogin.js.map

/***/ }),

/***/ 313:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpinnerloaderPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SpinnerloaderPage = /** @class */ (function () {
    function SpinnerloaderPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    SpinnerloaderPage.prototype.ionViewDidLoad = function () {
        debugger;
        console.log('SpinnerloaderPage');
        // let loading;
        // loading = this.loadingCtrl.create({ content: "Logging in ,please wait..." });
        // loading.present();
    };
    SpinnerloaderPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-spinnerloader',template:/*ion-inline-start:"E:\Ionic\Ionic-master\src\pages\spinnerloader\spinnerloader.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Ionic-Master</ion-title>\n    <ion-buttons end>\n      <button ion-button tappable>\n        <ion-icon name="notifications"></ion-icon>\n      </button>\n      <button ion-button tappable>\n        <ion-icon name="cog"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content fullscreen>\n  <ion-spinner name="dots" class="custom custom-dot svg"></ion-spinner>\n  <ion-card class="custombutton">\n  <button type="submit" >\n    <ion-spinner name="bubbles" class="custom custom-dot svg"></ion-spinner> Click me!\n    </button>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"E:\Ionic\Ionic-master\src\pages\spinnerloader\spinnerloader.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], SpinnerloaderPage);
    return SpinnerloaderPage;
}());

//# sourceMappingURL=spinnerloader.js.map

/***/ }),

/***/ 314:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BarqrcodePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__ = __webpack_require__(315);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_data_service_data_service__ = __webpack_require__(316);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var BarqrcodePage = /** @class */ (function () {
    function BarqrcodePage(navCtrl, navParams, barcodeScanner, dataService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.barcodeScanner = barcodeScanner;
        this.dataService = dataService;
        this.products = [];
        this.productFound = false;
        this.dataService.getProducts()
            .subscribe(function (response) {
            _this.products = response;
            console.log(_this.products);
        });
    }
    BarqrcodePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BarqrcodePage');
    };
    BarqrcodePage.prototype.scan = function () {
        var _this = this;
        this.selectedProduct = {};
        this.barcodeScanner.scan().then(function (barcodeData) {
            _this.selectedProduct = _this.products.find(function (product) { return product.plu === barcodeData.text; });
            if (_this.selectedProduct !== undefined) {
                _this.productFound = true;
                console.log(_this.selectedProduct);
            }
            else {
                _this.selectedProduct = {};
                _this.productFound = false;
            }
        }, function (err) {
        });
    };
    BarqrcodePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-barqrcode',template:/*ion-inline-start:"E:\Ionic\Ionic-master\src\pages\barqrcode\barqrcode.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Ionic-Master</ion-title>\n    <ion-buttons end>\n      <button ion-button tappable>\n        <ion-icon name="notifications"></ion-icon>\n      </button>\n      <button ion-button tappable>\n        <ion-icon name="cog"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n  The world is your oyster.\n  <p>\n    If you get lost, the <a href="http://ionicframework.com/docs/v2">docs</a> will be your guide.\n  </p>\n  <h1>\n    <button ion-button color="dark" full round (click)="scan()">Start Scan</button>\n  </h1>\n\n  <ion-card *ngIf="productFound">\n    <ion-card-header>\n      <h2>Price: $ {{selectedProduct.price}}</h2>\n    </ion-card-header>\n    <ion-card-content>\n      <ul>\n        <li>{{selectedProduct.plu}}</li>\n        <li>{{selectedProduct.name}}</li>\n        <li>{{selectedProduct.desc}}</li>\n      </ul>\n    </ion-card-content>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"E:\Ionic\Ionic-master\src\pages\barqrcode\barqrcode.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
            __WEBPACK_IMPORTED_MODULE_3__providers_data_service_data_service__["a" /* DataServiceProvider */]])
    ], BarqrcodePage);
    return BarqrcodePage;
}());

//# sourceMappingURL=barqrcode.js.map

/***/ }),

/***/ 316:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DataServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(166);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the DataServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var DataServiceProvider = /** @class */ (function () {
    function DataServiceProvider(http) {
        this.http = http;
        console.log('Hello DataServiceProvider Provider');
    }
    DataServiceProvider.prototype.getProducts = function () {
        return this.http.get('assets/data/products.json')
            .map(function (response) { return response.json(); });
    };
    DataServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], DataServiceProvider);
    return DataServiceProvider;
}());

//# sourceMappingURL=data-service.js.map

/***/ }),

/***/ 317:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpeechtotextPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_speech_recognition__ = __webpack_require__(318);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_text_to_speech__ = __webpack_require__(319);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var SpeechtotextPage = /** @class */ (function () {
    function SpeechtotextPage(navCtrl, navParams, speech, zone, tts) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.speech = speech;
        this.zone = zone;
        this.tts = tts;
        this.isListening = false;
        this.isgetting = false;
    }
    SpeechtotextPage.prototype.hasPermission = function () {
        return __awaiter(this, void 0, void 0, function () {
            var permission, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.speech.hasPermission()];
                    case 1:
                        permission = _a.sent();
                        console.log(permission);
                        return [2 /*return*/, permission];
                    case 2:
                        e_1 = _a.sent();
                        console.log(e_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    SpeechtotextPage.prototype.getPermission = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                try {
                    this.speech.requestPermission()
                        .then(function () { return _this.isgetting = true; }, function () { return console.log('Denied'); });
                }
                catch (e) {
                    console.log(e);
                }
                return [2 /*return*/];
            });
        });
    };
    SpeechtotextPage.prototype.listen = function () {
        this.getPermission();
        console.log('listen action triggered');
        // if (this.isListening) {
        //   this.speech.stopListening();
        //   this.toggleListenMode();
        //   return
        // }
        // this.toggleListenMode();
        // let _this = this;
        // this.speech.startListening()
        //   .subscribe(matches => {
        //     _this.zone.run(() => {
        //       _this.matches = matches;
        //     })
        //   }, error => console.error(error));
    };
    SpeechtotextPage.prototype.startlistning = function () {
        var _this = this;
        this.speech.startListening()
            .subscribe(function (matches) {
            _this.zone.run(function () {
                _this.matches = matches;
            });
        }, function (error) { return console.error(error); });
    };
    SpeechtotextPage.prototype.hearlisten = function () {
        this.tts.speak('Hello kiran')
            .then(function () { return console.log('Success'); })
            .catch(function (reason) { return console.log(reason); });
    };
    SpeechtotextPage.prototype.toggleListenMode = function () {
        this.isListening = this.isListening ? false : true;
        console.log('listening mode is now : ' + this.isListening);
    };
    SpeechtotextPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-speechtotext',template:/*ion-inline-start:"E:\Ionic\Ionic-master\src\pages\speechtotext\speechtotext.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Ionic-Master</ion-title>\n    <ion-buttons end>\n      <button ion-button tappable>\n        <ion-icon name="notifications"></ion-icon>\n      </button>\n      <button ion-button tappable>\n        <ion-icon name="cog"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <p>Is listening ? : {{ isListening }}</p>\n  <button ion-button (click)="getPermission()">Get Permission</button>\n  <button ion-button (click)="listen()">\n    <div>\n      <ion-icon name="mic">\n        <label>Speak to me</label>\n      </ion-icon>\n    </div>\n  </button>\n  <button ion-button (click)="hearlisten()">\n    <div>\n      <ion-icon name="txtmic">\n        <label>text to speech</label>\n      </ion-icon>\n    </div>\n  </button>\n  <ion-card>\n    <ion-card-content *ngFor="let match of matches">\n      <p>{{ match }}</p>\n    </ion-card-content>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"E:\Ionic\Ionic-master\src\pages\speechtotext\speechtotext.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_speech_recognition__["a" /* SpeechRecognition */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_text_to_speech__["a" /* TextToSpeech */]])
    ], SpeechtotextPage);
    return SpeechtotextPage;
}());

//# sourceMappingURL=speechtotext.js.map

/***/ }),

/***/ 320:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CameraPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__ = __webpack_require__(321);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_photo_library__ = __webpack_require__(322);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};






var CameraPage = /** @class */ (function () {
    function CameraPage(navCtrl, camera, alertCtrl, photoLibrary, document) {
        this.navCtrl = navCtrl;
        this.camera = camera;
        this.alertCtrl = alertCtrl;
        this.photoLibrary = photoLibrary;
        this.document = document;
        this.filter = 1;
        this.filters = {
            blur: { name: 'blur', max: '10', min: '0', value: 0, map: "px" },
            opacity: { name: 'opacity', max: 100, min: 1, value: '100', map: "%" },
            grayscale: { name: 'grayscale', max: 100, min: 0, value: 0, map: "%" },
            brightness: { name: 'brightness', max: 500, min: 25, value: 100, map: "%" },
            sepia: { name: 'sepia', max: '100', min: '0', value: '0', map: "%" }
        };
        this.options = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            allowEdit: true,
            mediaType: this.camera.MediaType.PICTURE
        };
    }
    CameraPage.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.image = new Image();
        this.image.src = 'assets/camera.jpg';
        this.canvasContext = this.getCanvas();
        this.image.onload = function () { return _this.canvasContext.drawImage(_this.image, 0, 0, 400, 400); };
    };
    CameraPage.prototype.getSnap = function () {
        var _this = this;
        this.camera.getPicture(this.options).then(function (imageData) {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64:
            var base64Image = 'data:image/jpeg;base64,' + imageData;
            //console.log(imageData);
            // this.showAlert("",imageData);
            _this.image.src = base64Image;
        }, function (err) {
            // Handle error
            _this.showAlert('Error', 'failed to click image');
        });
    };
    CameraPage.prototype.showAlert = function (title, message) {
        var alert = this.alertCtrl.create({
            title: title || "Alert",
            subTitle: message,
            buttons: ['OK']
        });
        alert.present();
    };
    CameraPage.prototype.applyFilterFromJs = function (filterProp, event) {
        switch (filterProp.name) {
            case "blur":
                this.filters.blur.value = event.value;
                break;
            case "opacity":
                this.filters.opacity.value = event.value;
                break;
            case "grayscale":
                this.filters.grayscale.value = event.value;
                break;
            case "brightness":
                this.filters.brightness.value = event.value;
                break;
            case "sepia":
                this.filters.sepia.value = event.value;
                break;
            default:
                break;
        }
        this.canvasContext.filter = "\n    blur(" + this.filters.blur.value + this.filters.blur.map + ")\n    opacity(" + this.filters.opacity.value + this.filters.opacity.map + ")\n    grayscale(" + this.filters.grayscale.value + this.filters.grayscale.map + ")\n    brightness(" + this.filters.brightness.value + this.filters.brightness.map + ")\n    sepia(" + this.filters.sepia.value + this.filters.sepia.map + ")\n  ";
        this.canvasContext.drawImage(this.image, 0, 0, 400, 800);
    };
    CameraPage.prototype.getCanvas = function () {
        var canvas = this.document.getElementById('canvas');
        var context = canvas.getContext('2d');
        return context;
    };
    CameraPage.prototype.save = function () {
        var album = 'MyAppName';
        var image = this.canvasContext.toDataURL();
        this.photoLibrary.saveImage(image, album);
    };
    CameraPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-camera',template:/*ion-inline-start:"E:\Ionic\Ionic-master\src\pages\camera\camera.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Camera\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <button ion-button full color="primary" (click)="getSnap()">Take Snap</button>\n    <button ion-button full  (click)="save()">Save</button>\n    <div class="center">\n    <canvas id="canvas"></canvas>\n    </div>\n    <ion-list  no-lines>\n    <div *ngFor="let filterProp of filters|keys">\n      <h3>{{filterProp.value.name | uppercase}}</h3>\n  <ion-range [min]="filterProp.value.min" [(ngModel)]="filterProp.value.value" [max]="filterProp.value.max"\n      (ionChange)="applyFilterFromJs(filterProp.value,$event)">\n       <ion-label range-left>{{filterProp.value.min}}</ion-label>\n      <ion-label range-right>{{filterProp.value.max}}</ion-label>\n  </ion-range>\n</div>\n</ion-list>\n</ion-content>\n'/*ion-inline-end:"E:\Ionic\Ionic-master\src\pages\camera\camera.html"*/
        }),
        __param(4, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["z" /* Inject */])(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["b" /* DOCUMENT */])),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_photo_library__["a" /* PhotoLibrary */], Object])
    ], CameraPage);
    return CameraPage;
}());

//# sourceMappingURL=camera.js.map

/***/ }),

/***/ 324:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(325);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(447);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);



Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["_14" /* enableProdMode */])();
Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 447:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export firebaseConfig */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__pages_bledetail_bledetail__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pages_blehome_blehome__ = __webpack_require__(287);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_googleauth_googleauth__ = __webpack_require__(288);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_tabpage_tabpage__ = __webpack_require__(290);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_ble__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_status_bar__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_component__ = __webpack_require__(495);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_home_home__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_contact_contact__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_about_about__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_tabs_tabs__ = __webpack_require__(531);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_login_login__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_Register_register__ = __webpack_require__(97);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__angular_http__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__angular_common_http__ = __webpack_require__(310);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_Model_model__ = __webpack_require__(309);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_google_plus__ = __webpack_require__(289);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_mapslocation_mapslocation__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_geolocation__ = __webpack_require__(306);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_device__ = __webpack_require__(307);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_facebook__ = __webpack_require__(312);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_facebooklogin_facebooklogin__ = __webpack_require__(311);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_spinnerloader_spinnerloader__ = __webpack_require__(313);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_barqrcode_barqrcode__ = __webpack_require__(314);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__providers_data_service_data_service__ = __webpack_require__(316);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__ionic_native_barcode_scanner__ = __webpack_require__(315);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_speechtotext_speechtotext__ = __webpack_require__(317);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__ionic_native_speech_recognition__ = __webpack_require__(318);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__ionic_native_text_to_speech__ = __webpack_require__(319);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_camera_camera__ = __webpack_require__(320);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__ionic_native_camera__ = __webpack_require__(321);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__ionic_native_photo_library__ = __webpack_require__(322);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pipes_keys_pipe__ = __webpack_require__(532);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__pages_list_list__ = __webpack_require__(308);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38_angularfire2__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39_angularfire2_database__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__providers_firebase_firebase__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41_angularfire2_firestore__ = __webpack_require__(533);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42_angularfire2_auth__ = __webpack_require__(300);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__pages_forgot_forgot__ = __webpack_require__(304);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



// import { HttpClient } from '@angular/common/http';



































// Import the AF2 Module






//import { AlertController } from 'ionic-angular';
// AF2 Settings
var firebaseConfig = {
    apiKey: "AIzaSyB4571MNAFMHuBSw1uzfq7SHZHEH_aoKZs",
    authDomain: "ionicmaster-7c02a.firebaseapp.com",
    databaseURL: "https://ionicmaster-7c02a.firebaseio.com",
    projectId: "ionicmaster-7c02a",
    storageBucket: "ionicmaster-7c02a.appspot.com",
    messagingSenderId: "109603024523"
};
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_5__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_11__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_list_list__["a" /* ListPage */], __WEBPACK_IMPORTED_MODULE_14__pages_tabs_tabs__["a" /* TabsPage */], __WEBPACK_IMPORTED_MODULE_13__pages_about_about__["a" /* AboutPage */], __WEBPACK_IMPORTED_MODULE_12__pages_contact_contact__["a" /* ContactPage */], __WEBPACK_IMPORTED_MODULE_3__pages_tabpage_tabpage__["a" /* TabPage */], __WEBPACK_IMPORTED_MODULE_15__pages_login_login__["a" /* LoginPage */], __WEBPACK_IMPORTED_MODULE_16__pages_Register_register__["a" /* RegisterPage */], __WEBPACK_IMPORTED_MODULE_19__pages_Model_model__["a" /* ModelPage */], __WEBPACK_IMPORTED_MODULE_2__pages_googleauth_googleauth__["a" /* GoogleauthPage */], __WEBPACK_IMPORTED_MODULE_21__pages_mapslocation_mapslocation__["a" /* MapslocationPage */], __WEBPACK_IMPORTED_MODULE_25__pages_facebooklogin_facebooklogin__["a" /* FacebookloginPage */],
                __WEBPACK_IMPORTED_MODULE_1__pages_blehome_blehome__["a" /* BlehomePage */], __WEBPACK_IMPORTED_MODULE_0__pages_bledetail_bledetail__["a" /* BledetailPage */], __WEBPACK_IMPORTED_MODULE_26__pages_spinnerloader_spinnerloader__["a" /* SpinnerloaderPage */], __WEBPACK_IMPORTED_MODULE_27__pages_barqrcode_barqrcode__["a" /* BarqrcodePage */], __WEBPACK_IMPORTED_MODULE_30__pages_speechtotext_speechtotext__["a" /* SpeechtotextPage */], __WEBPACK_IMPORTED_MODULE_33__pages_camera_camera__["a" /* CameraPage */], __WEBPACK_IMPORTED_MODULE_36__pipes_keys_pipe__["a" /* KeysPipe */], __WEBPACK_IMPORTED_MODULE_43__pages_forgot_forgot__["a" /* ForgotPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_17__angular_http__["c" /* HttpModule */], __WEBPACK_IMPORTED_MODULE_18__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/hometest/hometest.module#HomePageModule', name: 'HomePagetest', segment: 'hometest', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/testpage/testpage.module#TestpagePageModule', name: 'TestpagePage', segment: 'testpage', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_39_angularfire2_database__["b" /* AngularFireDatabaseModule */],
                __WEBPACK_IMPORTED_MODULE_38_angularfire2__["a" /* AngularFireModule */].initializeApp(firebaseConfig),
                __WEBPACK_IMPORTED_MODULE_41_angularfire2_firestore__["a" /* AngularFirestoreModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_6_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_11__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_list_list__["a" /* ListPage */], __WEBPACK_IMPORTED_MODULE_14__pages_tabs_tabs__["a" /* TabsPage */], __WEBPACK_IMPORTED_MODULE_13__pages_about_about__["a" /* AboutPage */], __WEBPACK_IMPORTED_MODULE_12__pages_contact_contact__["a" /* ContactPage */], __WEBPACK_IMPORTED_MODULE_3__pages_tabpage_tabpage__["a" /* TabPage */], __WEBPACK_IMPORTED_MODULE_15__pages_login_login__["a" /* LoginPage */], __WEBPACK_IMPORTED_MODULE_16__pages_Register_register__["a" /* RegisterPage */], __WEBPACK_IMPORTED_MODULE_19__pages_Model_model__["a" /* ModelPage */], __WEBPACK_IMPORTED_MODULE_2__pages_googleauth_googleauth__["a" /* GoogleauthPage */], __WEBPACK_IMPORTED_MODULE_21__pages_mapslocation_mapslocation__["a" /* MapslocationPage */], __WEBPACK_IMPORTED_MODULE_25__pages_facebooklogin_facebooklogin__["a" /* FacebookloginPage */],
                __WEBPACK_IMPORTED_MODULE_1__pages_blehome_blehome__["a" /* BlehomePage */], __WEBPACK_IMPORTED_MODULE_0__pages_bledetail_bledetail__["a" /* BledetailPage */], __WEBPACK_IMPORTED_MODULE_26__pages_spinnerloader_spinnerloader__["a" /* SpinnerloaderPage */], __WEBPACK_IMPORTED_MODULE_27__pages_barqrcode_barqrcode__["a" /* BarqrcodePage */], __WEBPACK_IMPORTED_MODULE_30__pages_speechtotext_speechtotext__["a" /* SpeechtotextPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_camera_camera__["a" /* CameraPage */], __WEBPACK_IMPORTED_MODULE_43__pages_forgot_forgot__["a" /* ForgotPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_14__pages_tabs_tabs__["a" /* TabsPage */], __WEBPACK_IMPORTED_MODULE_13__pages_about_about__["a" /* AboutPage */], __WEBPACK_IMPORTED_MODULE_12__pages_contact_contact__["a" /* ContactPage */], __WEBPACK_IMPORTED_MODULE_3__pages_tabpage_tabpage__["a" /* TabPage */], __WEBPACK_IMPORTED_MODULE_15__pages_login_login__["a" /* LoginPage */], __WEBPACK_IMPORTED_MODULE_16__pages_Register_register__["a" /* RegisterPage */], __WEBPACK_IMPORTED_MODULE_19__pages_Model_model__["a" /* ModelPage */], __WEBPACK_IMPORTED_MODULE_20__ionic_native_google_plus__["a" /* GooglePlus */], __WEBPACK_IMPORTED_MODULE_21__pages_mapslocation_mapslocation__["a" /* MapslocationPage */], __WEBPACK_IMPORTED_MODULE_25__pages_facebooklogin_facebooklogin__["a" /* FacebookloginPage */],
                __WEBPACK_IMPORTED_MODULE_1__pages_blehome_blehome__["a" /* BlehomePage */], __WEBPACK_IMPORTED_MODULE_0__pages_bledetail_bledetail__["a" /* BledetailPage */], __WEBPACK_IMPORTED_MODULE_27__pages_barqrcode_barqrcode__["a" /* BarqrcodePage */], __WEBPACK_IMPORTED_MODULE_28__providers_data_service_data_service__["a" /* DataServiceProvider */], __WEBPACK_IMPORTED_MODULE_29__ionic_native_barcode_scanner__["a" /* BarcodeScanner */], __WEBPACK_IMPORTED_MODULE_31__ionic_native_speech_recognition__["a" /* SpeechRecognition */], __WEBPACK_IMPORTED_MODULE_32__ionic_native_text_to_speech__["a" /* TextToSpeech */],
                { provide: __WEBPACK_IMPORTED_MODULE_5__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["c" /* IonicErrorHandler */] }, __WEBPACK_IMPORTED_MODULE_22__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_23__ionic_native_device__["a" /* Device */], __WEBPACK_IMPORTED_MODULE_24__ionic_native_facebook__["a" /* Facebook */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_ble__["a" /* BLE */], __WEBPACK_IMPORTED_MODULE_34__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_35__ionic_native_photo_library__["a" /* PhotoLibrary */], __WEBPACK_IMPORTED_MODULE_33__pages_camera_camera__["a" /* CameraPage */], __WEBPACK_IMPORTED_MODULE_43__pages_forgot_forgot__["a" /* ForgotPage */],
                __WEBPACK_IMPORTED_MODULE_40__providers_firebase_firebase__["a" /* FirebaseProvider */],
                __WEBPACK_IMPORTED_MODULE_42_angularfire2_auth__["a" /* AngularFireAuth */]
            ],
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 495:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__pages_home_home__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pages_login_login__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_firebase_firebase__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_blehome_blehome__ = __webpack_require__(287);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_mapslocation_mapslocation__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_googleauth_googleauth__ = __webpack_require__(288);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_tabpage_tabpage__ = __webpack_require__(290);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_status_bar__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_splash_screen__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_list_list__ = __webpack_require__(308);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_Register_register__ = __webpack_require__(97);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_facebooklogin_facebooklogin__ = __webpack_require__(311);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_spinnerloader_spinnerloader__ = __webpack_require__(313);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_barqrcode_barqrcode__ = __webpack_require__(314);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_speechtotext_speechtotext__ = __webpack_require__(317);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_camera_camera__ = __webpack_require__(320);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












// import { TabsPage } from '../pages/tabs/tabs';






var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, firebaseProvider) {
        this.firebaseProvider = firebaseProvider;
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Home', component: __WEBPACK_IMPORTED_MODULE_0__pages_home_home__["a" /* HomePage */] },
            { title: 'Tab', component: __WEBPACK_IMPORTED_MODULE_6__pages_tabpage_tabpage__["a" /* TabPage */] },
            { title: 'Login', component: __WEBPACK_IMPORTED_MODULE_1__pages_login_login__["a" /* LoginPage */] },
            { title: 'Register', component: __WEBPACK_IMPORTED_MODULE_12__pages_Register_register__["a" /* RegisterPage */] },
            { title: 'List', component: __WEBPACK_IMPORTED_MODULE_11__pages_list_list__["a" /* ListPage */] },
            { title: 'Google Sign in', component: __WEBPACK_IMPORTED_MODULE_5__pages_googleauth_googleauth__["a" /* GoogleauthPage */] },
            { title: 'MapLocation', component: __WEBPACK_IMPORTED_MODULE_4__pages_mapslocation_mapslocation__["a" /* MapslocationPage */] },
            { title: 'facebooklogin', component: __WEBPACK_IMPORTED_MODULE_13__pages_facebooklogin_facebooklogin__["a" /* FacebookloginPage */] },
            { title: 'Ble', component: __WEBPACK_IMPORTED_MODULE_3__pages_blehome_blehome__["a" /* BlehomePage */] },
            { title: 'SpinnerLoader', component: __WEBPACK_IMPORTED_MODULE_14__pages_spinnerloader_spinnerloader__["a" /* SpinnerloaderPage */] },
            { title: 'BarQrcode', component: __WEBPACK_IMPORTED_MODULE_15__pages_barqrcode_barqrcode__["a" /* BarqrcodePage */] },
            { title: 'Speechtotext', component: __WEBPACK_IMPORTED_MODULE_16__pages_speechtotext_speechtotext__["a" /* SpeechtotextPage */] },
            { title: 'Camera & Filter', component: __WEBPACK_IMPORTED_MODULE_17__pages_camera_camera__["a" /* CameraPage */] },
        ];
        this.viewDidLoad();
    }
    MyApp.prototype.viewDidLoad = function () {
        var _this = this;
        debugger;
        this.firebaseProvider.afAuth.authState.subscribe(function (user) {
            if (user && user.emailVerified) {
                _this.SuperUser = user;
                _this.firebaseProvider.setUsername();
                _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_0__pages_home_home__["a" /* HomePage */]);
            }
            else {
                _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_1__pages_login_login__["a" /* LoginPage */]);
            }
        }, function (error) {
            _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_1__pages_login_login__["a" /* LoginPage */]);
        });
    };
    MyApp.prototype.openPage = function (page) {
        this.nav.setRoot(page.component);
    };
    // openPage() {
    //   debugger;
    //   this.firebaseProvider.afAuth.authState.subscribe( user => {
    //     if (user && user.emailVerified) {
    //        this.firebaseProvider.setFreshUser(user);
    //        this.rootPage=HomePage
    //        this.SuperUser=user;
    //       (this.uber = this.firebaseProvider.findEmail(this.SuperUser.email.toLowerCase()))
    //       .subscribe ((res: User[]) => {
    //         console.log(res);
    //         this.username = res[0].Username;
    //       });
    //       // this.nav.setRoot(this.rootPage);
    //     } else {
    //       this.rootPage=LoginPage;
    //       this.nav.setRoot(this.rootPage);
    //     }
    //   },
    //  () => {
    //   this.rootPage=LoginPage;
    //   this.nav.setRoot(this.rootPage);
    //   console.log("logout error");
    //  }
    //  );
    // }
    MyApp.prototype.logout = function () {
        var _this = this;
        debugger;
        this.firebaseProvider.signOut().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            console.log("logout");
            _this.firebaseProvider.username = "";
            _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_1__pages_login_login__["a" /* LoginPage */]);
        }, function (error) {
            console.log("logout error");
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_7__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_8_ionic_angular__["g" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_8_ionic_angular__["g" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_7__angular_core__["m" /* Component */])({template:/*ion-inline-start:"E:\Ionic\Ionic-master\src\app\app.html"*/'<ion-menu [content]="content">\n  <ion-header>\n      <ion-toolbar class="user-profile" [class.hide]="firebaseProvider.username.length==0">\n          <ion-grid>\n            <ion-row>\n              <ion-col col-4>\n                  <div class="user-avatar">\n                    <img src="../assets/imgs/avatar.jpeg">\n                  </div>\n              </ion-col>\n              <ion-col padding-top col-8>\n                <h2 ion-text class="no-margin bold text-white">\n                 {{firebaseProvider.username}}\n                </h2>\n                <span ion-text color="light">Customer</span>\n              </ion-col>\n            </ion-row>\n            <ion-row no-padding class="other-data">\n              <ion-col no-padding class="column">\n                <button ion-button icon-left small full color="light" menuClose disabled>\n                  <ion-icon name="contact"></ion-icon>\n                  Edit Profile\n                </button>\n              </ion-col>\n              <ion-col no-padding class="column">\n                <button ion-button icon-left small full color="light" menuClose (click)="logout()">\n                  <ion-icon name="log-out"></ion-icon>\n                  Log Out\n                </button>\n              </ion-col>\n            </ion-row>\n\n          </ion-grid>\n\n        </ion-toolbar>\n\n    <!-- <ion-toolbar>\n      <ion-title>Menu</ion-title>\n    </ion-toolbar> -->\n  </ion-header>\n\n  <ion-content>\n    <ion-list>\n      <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n        {{p.title}}\n      </button>\n    </ion-list>\n  </ion-content>\n\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>\n'/*ion-inline-end:"E:\Ionic\Ionic-master\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_8_ionic_angular__["j" /* Platform */], __WEBPACK_IMPORTED_MODULE_9__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_10__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_2__providers_firebase_firebase__["a" /* FirebaseProvider */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 518:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return User; });
var User = /** @class */ (function () {
    function User() {
    }
    return User;
}());

//# sourceMappingURL=user.model.js.map

/***/ }),

/***/ 531:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__about_about__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contact_contact__ = __webpack_require__(162);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TabsPage = /** @class */ (function () {
    function TabsPage() {
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_1__about_about__["a" /* AboutPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_2__contact_contact__["a" /* ContactPage */];
    }
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"E:\Ionic\Ionic-master\src\pages\tabs\tabs.html"*/'<!-- <ion-tabs>\n\n  <ion-tab [root]="tab2Root" tabTitle="About" tabIcon="information-circle"></ion-tab>\n\n  <ion-tab [root]="tab3Root" tabTitle="Contact" tabIcon="contacts"></ion-tab>\n\n</ion-tabs> -->\n\n'/*ion-inline-end:"E:\Ionic\Ionic-master\src\pages\tabs\tabs.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 532:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return KeysPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var KeysPipe = /** @class */ (function () {
    function KeysPipe() {
    }
    KeysPipe.prototype.transform = function (value, args) {
        var keys = [];
        for (var key in value) {
            keys.push({ key: key, value: value[key] });
        }
        return keys;
    };
    KeysPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Pipe */])({ name: 'keys' })
    ], KeysPipe);
    return KeysPipe;
}());

//# sourceMappingURL=keys.pipe.js.map

/***/ }),

/***/ 54:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FirebaseProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_database__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__ = __webpack_require__(300);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase__ = __webpack_require__(513);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_firebase__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




/*
  Generated class for the FirebaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var FirebaseProvider = /** @class */ (function () {
    function FirebaseProvider(afd, afAuth) {
        var _this = this;
        this.afd = afd;
        this.afAuth = afAuth;
        this.username = "";
        this.isRegistredUser = false;
        afAuth.authState.subscribe(function (user) {
            _this.Superuser = user;
        });
        debugger;
        this.db_users = this.afd.list('/User').valueChanges();
        //console.log(""+this.usert.count);
    }
    FirebaseProvider.prototype.getShoppingItems = function () {
        return this.db_users;
    };
    FirebaseProvider.prototype.findEmail = function (email) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, (this.main_users = this.afd.list('/User', function (ref) { return ref.orderByChild('Email').equalTo(email); }).valueChanges())];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    FirebaseProvider.prototype.addUser = function (name) {
        debugger;
        return this.afd.list('/User/').push(name);
    };
    FirebaseProvider.prototype.removeItem = function (id) {
        this.afd.list('/User/').remove(id);
    };
    FirebaseProvider.prototype.signInWithEmail = function (credentials) {
        console.log('Sign in with email');
        return this.afAuth.auth.signInWithEmailAndPassword(credentials.email, credentials.password);
    };
    FirebaseProvider.prototype.signUp = function (credentials) {
        return this.afAuth.auth.createUserWithEmailAndPassword(credentials.Email, credentials.password);
    };
    FirebaseProvider.prototype.signOut = function () {
        this.Superuser = null;
        return this.afAuth.auth.signOut();
    };
    FirebaseProvider.prototype.getEmail = function () {
        return this.Superuser && this.Superuser.email;
    };
    FirebaseProvider.prototype.getFreshUser = function () {
        var _this = this;
        this.afAuth.authState.subscribe(function (user) {
            if (user) {
                _this.Superuser = user;
                return _this.Superuser;
            }
            else {
                return null;
            }
        }, function () {
            return null;
        });
    };
    FirebaseProvider.prototype.getUser = function () {
        return this.Superuser;
    };
    FirebaseProvider.prototype.setUsername = function () {
        var _this = this;
        if (this.Superuser) {
            this.findEmail(this.Superuser.email).then(function (ref) {
                var uber = ref;
                ////
                uber.subscribe(function (res) {
                    console.log(res);
                    var m_user = res;
                    if (m_user.length !== 0) {
                        _this.username = res[0].Username;
                        _this.isRegistredUser = true;
                    }
                }, function () {
                    _this.isRegistredUser = true;
                });
            });
        }
    };
    FirebaseProvider.prototype.signInWithGoogle = function () {
        console.log('Sign in with google');
        return this.oauthSignIn(new __WEBPACK_IMPORTED_MODULE_3_firebase__["auth"].GoogleAuthProvider());
    };
    FirebaseProvider.prototype.oauthSignIn = function (provider) {
        var _this = this;
        if (!window.cordova) {
            return this.afAuth.auth.signInWithPopup(provider);
        }
        else {
            return this.afAuth.auth.signInWithRedirect(provider)
                .then(function () {
                return _this.afAuth.auth.getRedirectResult().then(function (result) {
                    // This gives you a Google Access Token.
                    // You can use it to access the Google API.
                    var token = result.credential;
                    // The signed-in user info.
                    var user = result.user;
                    console.log(token, user);
                    return user;
                }).catch(function (error) {
                    // Handle Errors here.
                    alert(error.message);
                    return error;
                });
            });
        }
    };
    FirebaseProvider.prototype.resetPassword = function (email) {
        return this.afAuth.auth.sendPasswordResetEmail(email);
    };
    FirebaseProvider.prototype.sendEmailVerificationLink = function (user) {
        var actionCodeSettings = { "url": "http://localhost:8100/" };
        return user.sendEmailVerification(actionCodeSettings);
    };
    FirebaseProvider.prototype.sendEmailLink = function (email) {
        var actionCodeSettings = { "url": "http://localhost:8100/" };
        return this.afAuth.auth.sendSignInLinkToEmail(email, actionCodeSettings);
    };
    FirebaseProvider.prototype.loginStatus = function () {
        var _this = this;
        this.afAuth.authState.subscribe(function (user) {
            if (user && user.emailVerified) {
                // this.firebaseProvider.setFreshUser(user);
            }
            else {
                _this.Superuser = null;
                _this.db_users = null;
                _this.main_users = null;
                _this.username = "";
                _this.isRegistredUser = false;
            }
        }, function (error) {
        });
    };
    FirebaseProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__["a" /* AngularFireAuth */]])
    ], FirebaseProvider);
    return FirebaseProvider;
}());

//# sourceMappingURL=firebase.js.map

/***/ }),

/***/ 71:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_firebase_firebase__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_status_bar__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__ = __webpack_require__(93);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HomePage = /** @class */ (function () {
    function HomePage(platform, nav, statusBar, splashScreen, firebaseProvider) {
        this.nav = nav;
        this.firebaseProvider = firebaseProvider;
        platform.ready().then(function () {
            statusBar.styleDefault();
            splashScreen.hide();
        });
        this.openPage();
    }
    HomePage.prototype.openPage = function () {
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"E:\Ionic\Ionic-master\src\pages\home\home.html"*/'<!-- <ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Ionic-Master</ion-title>\n  </ion-navbar>\n</ion-header> -->\n<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Ionic-Master</ion-title>\n    <ion-buttons end>\n      <button ion-button tappable>\n        <ion-icon name="notifications"></ion-icon>\n      </button>\n      <button ion-button tappable>\n        <ion-icon name="cog"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content  >\n    <div style="width: 100%;text-align: center; position: relative; ">\n\n      <video id="myVideo" width="100%" height="100%" autoplay="1" muted="">\n        <source src="https://cdn2.hubspot.net/hubfs/4234992/Cimconlighting-Feb2018/Videos/home-video-2.mp4?autoplay=1&mute=1&t=1534498051257" type="video/mp4">\n            Your browser does not support the video tag.</video>\n      <div style="position: absolute;\n                  top: 16px;\n                  position: absolute;\n                  left: 0;\n                  width: 100%;\n                  text-align: center;\n                  font-size: 18px;\n                  font-size: 18px;" >\n            <h1 style="color: #ffffff"  >\n                You should work with us!\n            </h1>\n            <h4 style="color: #ffffff">\n                XDuce is one of the emerging software companies located in Ahmedabad, India.<br> We offer mobile, web, customized software development and UI/UX design services.\n            </h4>\n          </div>\n\n\n  </div>\n\n</ion-content>\n'/*ion-inline-end:"E:\Ionic\Ionic-master\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* Platform */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_0__providers_firebase_firebase__["a" /* FirebaseProvider */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 96:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Register_register__ = __webpack_require__(97);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_firebase_firebase__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__home_home__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__forgot_forgot__ = __webpack_require__(304);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var LoginPage = /** @class */ (function () {
    function LoginPage(firebaseProvider, navCtrl, http, afDatabase) {
        //this.user = this.firebaseProvider.getShoppingItems();
        this.firebaseProvider = firebaseProvider;
        this.navCtrl = navCtrl;
        this.http = http;
        this.afDatabase = afDatabase;
        this.apiUrl = 'http://72.249.170.12/BluetoothApi/api/Login/Login';
        this.passwordType = 'password';
        this.paIcon = 'eye-off';
    }
    //https://www.djamware.com/post/59924f9080aca768e4d2b12e/ionic-3-consuming-rest-api-using-new-angular-43-httpclient
    LoginPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        setTimeout(function () {
            _this.email.setFocus();
        }, 500);
    };
    // login(): void {
    //   const obj = {
    //     "UserName":"admin",
    //     "Password":"admin"
    //    }
    // var headers = new Headers();
    //     headers.set('Content-type','application/json');
    //       console.log('doing'+ JSON.stringify(obj));
    //       debugger;
    //        const uri = 'http://72.249.170.12/HRDCServiceOnline/user.svc/UserLogin';
    //        this.http.post(uri,JSON.stringify(obj),{headers})
    //       .subscribe(res => {
    //         console.log("done");
    //         alert('done');
    //       }, (err) => {
    //         console.log(err);
    //       });
    // }
    LoginPage.prototype.login = function () {
        var _this = this;
        debugger;
        if (!this.username) {
            return;
        }
        var credentials = {
            email: this.username,
            password: this.password
        };
        this.firebaseProvider.signInWithEmail(credentials)
            .then(function (user) {
            if (user.user.emailVerified) {
                _this.firebaseProvider.Superuser = user.user;
                var pre_user = user.user;
                if (pre_user) {
                    _this.firebaseProvider.findEmail(pre_user.email).then(function (ref) {
                        _this.uber = ref;
                        ////
                        _this.uber.subscribe(function (res) {
                            console.log(res);
                            var m_user = res;
                            if (m_user.length === 0) {
                                _this.firebaseProvider.isRegistredUser = false;
                                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_0__Register_register__["a" /* RegisterPage */], { "email": pre_user.email });
                            }
                            else {
                                _this.firebaseProvider.username = res[0].Username;
                                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__home_home__["a" /* HomePage */]);
                            }
                        }, function () {
                            _this.firebaseProvider.isRegistredUser = false;
                            return false;
                        });
                    });
                }
                ////////////////
            }
            else {
                alert("Email not varifired.Please check your mailbox");
                _this.firebaseProvider.sendEmailVerificationLink(user.user).then(function (ref) {
                }, function (error) {
                    alert(error.message);
                });
            }
        }, function (error) { alert(error.message); _this.loginError = error.message; });
    };
    LoginPage.prototype.signup = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_0__Register_register__["a" /* RegisterPage */], { "email": "null" });
    };
    LoginPage.prototype.hideShowPassword = function () {
        this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
        this.paIcon = this.paIcon === 'eye-off' ? 'eye' : 'eye-off';
    };
    LoginPage.prototype.forgotPassword = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__forgot_forgot__["a" /* ForgotPage */]);
    };
    LoginPage.prototype.loginWithGoogle = function () {
        var _this = this;
        debugger;
        this.firebaseProvider.signInWithGoogle()
            .then(function (user) {
            if (user.user.emailVerified) {
                _this.firebaseProvider.Superuser = user.user;
                var pre_user = user.user;
                if (pre_user) {
                    _this.firebaseProvider.findEmail(pre_user.email).then(function (ref) {
                        _this.uber = ref;
                        ////
                        _this.uber.subscribe(function (res) {
                            console.log(res);
                            var m_user = res;
                            if (m_user.length === 0) {
                                _this.firebaseProvider.isRegistredUser = false;
                                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_0__Register_register__["a" /* RegisterPage */], { "email": pre_user.email });
                            }
                            else {
                                _this.firebaseProvider.username = res[0].Username;
                                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__home_home__["a" /* HomePage */]);
                            }
                        }, function () {
                            _this.firebaseProvider.isRegistredUser = false;
                            return false;
                        });
                    });
                }
            }
            else {
                alert("Email not varifired.Please check your mailbox");
            }
        }, function (error) { return console.log(error.message); });
    };
    LoginPage.prototype.saveData = function () {
        var obj = {
            UserName: "N/A",
            Password: "N/A",
            Name: "N/A",
            Email: this.firebaseProvider.getEmail()
        };
        this.firebaseProvider.addUser(obj);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_8" /* ViewChild */])('email'),
        __metadata("design:type", Object)
    ], LoginPage.prototype, "email", void 0);
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"E:\Ionic\Ionic-master\src\pages\login\login.html"*/'\n<ion-content padding class="login auth-page">\n    <div class="login-content">\n      <!-- Logo -->\n    <div padding-horizontal text-center class="animated fadeInDown">\n        <div class="logo"></div>\n        <h2 ion-text class="text-primary">\n          <strong>Ionic 3</strong> Start Theme\n        </h2>\n      </div>\n  <ion-row>\n  </ion-row>\n  <div >\n    <form #loginForm="ngForm"  autocomplete="off">\n      <ion-row>\n        <ion-col>\n          <ion-list inset>\n            <ion-item>\n                <ion-label floating>\n                    <ion-icon name="mail"\n                     item-start\n                     class="text-primary">\n                    </ion-icon>\n                    Email\n                  </ion-label>\n              <ion-input\n               name="username"\n                id="loginField"\n                type="email"\n                 required\n                  [(ngModel)]=\'username\'\n                 #email></ion-input>\n            </ion-item>\n            <ion-item>\n                  <ion-label floating>\n                    <ion-icon\n                    name="lock"\n                    item-start\n                    class="text-primary">\n                  </ion-icon>\n                    Password\n                  </ion-label>\n              <ion-input name="password"\n                      id="passwordField"\n                      [(ngModel)]=\'password\'\n                      [type]="passwordType"\n                      required\n                     ></ion-input>\n              <button ion-button clear item-end (click)=\'hideShowPassword()\' style="top: 22px !important;">\n                  <ion-icon [name]="paIcon" style="font-size:22px;"></ion-icon>\n               </button>\n            </ion-item>\n          </ion-list>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <div *ngIf="error" class="alert alert-danger">{{ error }}</div>\n          <button ion-button class="submit-btn" full type="submit" (click)="login()"\n                  [disabled]="!loginForm.form.valid">Login\n          </button>\n        </ion-col>\n      </ion-row>\n      <ion-row  class="right-text">\n        <div>\n          <p>\n              <button ion-button  clear (click)="forgotPassword()">\n\n                  Forgot password?\n                </button>\n\n          </p>\n        </div>\n      </ion-row>\n      <B><div style="text-align:center">If you\'re a new user, please sign up.</div></B>\n      <ion-row class="right-text">\n          <ion-list  class="right-text">\n\n          <button  ion-button icon-left block clear (click)="loginWithGoogle()">\n            <ion-icon name="logo-google"></ion-icon>\n            Log in with Google\n          </button>\n\n          <button ion-button icon-left block clear (click)="signup()">\n            <ion-icon name="person-add"></ion-icon>\n            Sign up with Email\n          </button>\n        </ion-list>\n      </ion-row>\n\n\n    </form>\n    See Here:\n    <div><ul *ngFor="let profile of uber | async">\n      <li> {{ profile.Email}}:{{ profile.Name}} </li>\n   </ul></div>\n  </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"E:\Ionic\Ionic-master\src\pages\login\login.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__providers_firebase_firebase__["a" /* FirebaseProvider */], __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["a" /* AngularFireDatabase */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 97:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__home_home__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__login_login__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__model_user_model__ = __webpack_require__(518);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__(166);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_toPromise__ = __webpack_require__(520);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_toPromise__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_firebase_firebase__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import {HttpParams} from '@angular/common/http/src/params';
// import { Component, ViewChild } from '@angular/core';
// import { Http, Headers,  } from '@angular/http';





// kimport {HttpParams} from '@angular/common/http/src/params';
//import { AlertController } from '../../../node_modules/ionic-angular';


var RegisterPage = /** @class */ (function () {
    function RegisterPage(navParams, navCtrl, http, firebaseProvider) {
        this.navParams = navParams;
        this.navCtrl = navCtrl;
        this.http = http;
        this.firebaseProvider = firebaseProvider;
        this.Name = "";
        this.password = "";
        this.Username = "";
        this.Email = "";
        this.u_email = "";
        this.flag = false;
        this.apiUrl = 'http://72.249.170.12/BluetoothApi/api/Login/Login';
        if (navParams.get("email") && navParams.get("email") !== "null") {
            debugger;
            this.u_email = this.firebaseProvider.Superuser.email.toLowerCase();
            this.Email = this.u_email;
            this.password = "null";
            if (this.u_email && this.u_email.length !== 0) {
                this.flag = true;
            }
        }
    }
    //https://www.djamware.com/post/59924f9080aca768e4d2b12e/ionic-3-consuming-rest-api-using-new-angular-43-httpclient
    // ionViewDidLoad(): void {
    //   setTimeout(() => {
    //     this.email.setFocus();
    //   }, 500);
    // }
    // register(): any {
    //   const obj = {
    //     UserName:this.Username ,
    //     Password:this.password,
    //     Name:this.Name,
    //     Email:this.Email
    //   }
    //   var headers = new Headers();
    //   headers.set('Content-type','application/json');
    //     console.log('doing'+ JSON.stringify(obj));
    //     debugger;
    //      const uri = 'http://72.249.170.12/BluetoothApi/api/Login/Login';
    //      this.http.post(uri,JSON.stringify(obj),{headers})
    //     .subscribe(res => {
    //       console.log("done");
    //       alert('done');
    //     }, (err) => {
    //       console.log(err);
    //     });
    // }
    // addUSer(){
    //   var obj : User;
    //    obj = new User();
    //    if(this.flag)
    //   {
    //     obj.Email= this.u_email;
    //     obj.LoginType= "gmail";
    //     obj.Password= "null";
    //   }
    //    else
    //   {
    //     obj.Email= this.u_email;
    //     obj.LoginType= "email";
    //     obj.Password= this.password;
    //   }
    //    obj.Name= this.Name;
    //    obj.Username= this.Username;
    //   this.firebaseProvider.addUser(obj);
    // }
    //{}
    // newSongRef.set({
    //   id: newSongRef.key,
    //   title: data.title
    // });
    // let prompt = this.alertCtrl.create({
    //   title: 'Submit the details',
    //   message: "Are you confirmed to submit ? ",
    //   inputs: [
    //     {
    //       name: 'title',
    //       placeholder: 'Title'
    //     },
    //   ],
    //   buttons: [
    //     {
    //       text: 'Cancel',
    //       handler: data => {
    //         console.log('Cancel clicked');
    //       }
    //     },
    //     {
    //       text: 'Save',
    //       handler: data => {
    //         const obj = {
    //           UserName:this.Username ,
    //           Password:this.password,
    //           Name:this.Name,
    //           Email:this.Email
    //         }
    //         const newSongRef = this.user.push(obj);
    //          console.log(""+newSongRef.key+","+data.title);
    //         // newSongRef.set({
    //         //   id: newSongRef.key,
    //         //   title: data.title
    //         // });
    //       }
    //     }
    //   ]
    // });
    // prompt.present();
    //}
    RegisterPage.prototype.signup = function () {
        var _this = this;
        debugger;
        if (this.flag) {
            this.saveData();
        }
        else {
            var credentials = {
                Email: this.Email.toLowerCase(),
                password: this.password
            };
            this.firebaseProvider.signUp(credentials).then(function (user) {
                _this.firebaseProvider.sendEmailVerificationLink(user.user).then(function (ref) {
                    _this.saveData();
                }, function (error) {
                    alert(error.message);
                });
            }, function (error) {
                alert(error.message);
                _this.signupError = error.message;
            });
        }
    };
    RegisterPage.prototype.saveData = function () {
        var _this = this;
        var obj;
        obj = new __WEBPACK_IMPORTED_MODULE_2__model_user_model__["a" /* User */]();
        if (this.flag) {
            obj.Email = this.u_email.toLowerCase();
            obj.LoginType = "gmail";
            obj.Password = "null";
        }
        else {
            obj.Email = this.Email.toLowerCase();
            obj.LoginType = "email";
            obj.Password = this.password;
        }
        obj.Name = this.Name;
        obj.Username = this.Username;
        this.firebaseProvider.addUser(obj).then(function () {
            if (!_this.flag) {
                alert("Please varifiy your email address.Check your mailbox for that.");
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_1__login_login__["a" /* LoginPage */]);
            }
            else {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_0__home_home__["a" /* HomePage */]);
            }
        }, function (error) {
            alert(error.message);
        });
        //console.log("DataSaved:"+newSongRef.key+"--"+obj);
        // newSongRef.set({
        //   id: newSongRef.key,
        //   title: data.title
        // });
    };
    RegisterPage.prototype.valid = function () {
        if (this.Email.length !== 0)
            return false;
        if (this.Name.length !== 0)
            return false;
        if (this.Username.length !== 0)
            return false;
        else
            return true;
    };
    RegisterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["m" /* Component */])({
            selector: 'page-register',template:/*ion-inline-start:"E:\Ionic\Ionic-master\src\pages\Register\register.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Ionic-Master</ion-title>\n    <ion-buttons end>\n      <button ion-button tappable>\n        <ion-icon name="notifications"></ion-icon>\n      </button>\n      <button ion-button tappable>\n        <ion-icon name="cog"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content padding>\n  <ion-row>\n  </ion-row>\n  <div>\n    <form #registerForm="ngForm"  autocomplete="off">\n      <!-- //(ngSubmit)="addUSer()" -->\n      <ion-row>\n        <ion-col>\n          <ion-list inset>\n             <ion-item>\n                <ion-label floating>\n                    <ion-icon name="contact" item-start class="text-primary">\n                    </ion-icon>\n                       Full Name\n                  </ion-label>\n              <ion-input name="Name" id="nameField" type="text"  required [(ngModel)]="Name"></ion-input>\n            </ion-item>\n            <ion-item [class.hide]="!flag">\n                <ion-label floating>\n                    <ion-icon name="mail" item-start class="text-primary">\n                    </ion-icon>\n                    Email\n                  </ion-label>\n              <ion-input  name="u_email" id="emailField" type="email" [(ngModel)]="u_email" #email\n                         [disabled]="true"></ion-input>\n            </ion-item>\n            <ion-item  [class.hide]="flag">\n                  <ion-label floating>\n                     <ion-icon name="mail" item-start class="text-primary">\n                     </ion-icon>\n                      Email\n                  </ion-label>\n                  <ion-input  name="Email" id="emailField" type="email" required [(ngModel)]="Email" #email>\n                  </ion-input>\n            </ion-item>\n            <ion-item>\n                <ion-label floating>\n                    <ion-icon name="contact"\n                     item-start\n                     class="text-primary">\n                    </ion-icon>\n                      Username\n                  </ion-label>\n                <ion-input  name="Username" id="usernameField" type="text" required [(ngModel)]="Username"></ion-input>\n              </ion-item>\n            <ion-item [class.hide]="flag">\n                <ion-label floating>\n                    <ion-icon  name="lock" item-start\n                    class="text-primary">\n                  </ion-icon>\n                    Password\n                  </ion-label>\n            <ion-input  name="password" id="passwordField"\n                         type="password"  [(ngModel)]="password"></ion-input>\n            </ion-item>\n            </ion-list>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <!-- <div *ngIf="error" class="alert alert-danger">{{ error }}</div> -->\n          <button ion-button class="submit-btn" full type="submit" (click)="signup()"\n                  [disabled]="!registerForm.form.valid">Register\n          </button>\n        </ion-col>\n      </ion-row>\n    </form>\n    See Here:\n      <div><ul *ngFor="let profile of uber | async">\n      <li> {{ profile.Email}}:{{ profile.Name}} </li>\n      </ul>\n      </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"E:\Ionic\Ionic-master\src\pages\Register\register.html"*/
        }),
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_8_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_8_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_4__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_7__providers_firebase_firebase__["a" /* FirebaseProvider */]])
    ], RegisterPage);
    return RegisterPage;
}());

//# sourceMappingURL=register.js.map

/***/ })

},[324]);
//# sourceMappingURL=main.js.map