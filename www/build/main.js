webpackJsonp([1],{

/***/ 159:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AboutPage = /** @class */ (function () {
    function AboutPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    AboutPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-about',template:/*ion-inline-start:"/Users/vaidahi/Desktop/KheloJiyoiOS/khelojiyoiosionic/src/pages/about/about.html"*/'<ion-header padding>\n  <ion-title>\n    About\n  </ion-title>\n</ion-header>\n\n<ion-content padding>\n   <p>\n    If you get lost, the <a href="http://ionicframework.com/docs/v2">docs</a> will be your guide.\n  </p>\n\n\n</ion-content>\n'/*ion-inline-end:"/Users/vaidahi/Desktop/KheloJiyoiOS/khelojiyoiosionic/src/pages/about/about.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */]])
    ], AboutPage);
    return AboutPage;
}());

//# sourceMappingURL=about.js.map

/***/ }),

/***/ 160:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ContactPage = /** @class */ (function () {
    function ContactPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    ContactPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-contact',template:/*ion-inline-start:"/Users/vaidahi/Desktop/KheloJiyoiOS/khelojiyoiosionic/src/pages/contact/contact.html"*/'<ion-header>\n\n  <ion-title>\n\n    Contact\n\n  </ion-title>\n\n</ion-header>\n\n<ion-content>\n\n  <ion-list>\n\n    <ion-list-header>Follow us on Twitter</ion-list-header>\n\n    <ion-item>\n\n      <ion-icon name="ionic" item-start></ion-icon>\n\n      geoduck\n\n    </ion-item>\n\n  </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"/Users/vaidahi/Desktop/KheloJiyoiOS/khelojiyoiosionic/src/pages/contact/contact.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */]])
    ], ContactPage);
    return ContactPage;
}());

//# sourceMappingURL=contact.js.map

/***/ }),

/***/ 164:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Register_register__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_firebase_firebase__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__home_home__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__forgot_forgot__ = __webpack_require__(303);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_google_plus__ = __webpack_require__(304);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__environments_environment__ = __webpack_require__(522);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_data_service_data_service__ = __webpack_require__(166);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_facebook__ = __webpack_require__(168);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};












var LoginPage = /** @class */ (function () {
    function LoginPage(firebaseProvider, navCtrl, http, gPlus, afDatabase, api, url, facebook, loadingCtrl) {
        this.firebaseProvider = firebaseProvider;
        this.navCtrl = navCtrl;
        this.http = http;
        this.gPlus = gPlus;
        this.afDatabase = afDatabase;
        this.api = api;
        this.url = url;
        this.facebook = facebook;
        this.loadingCtrl = loadingCtrl;
        this.apiUrl = 'http://72.249.170.12/BluetoothApi/api/Login/Login';
        this.passwordType = 'password';
        this.paIcon = 'eye-off';
        //this.user = this.firebaseProvider.getShoppingItems();    
        this.loading = this.loadingCtrl.create({ content: "please wait..." });
    }
    //https://www.djamware.com/post/59924f9080aca768e4d2b12e/ionic-3-consuming-rest-api-using-new-angular-43-httpclient
    LoginPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        setTimeout(function () {
            _this.email.setFocus();
        }, 500);
    };
    LoginPage.prototype.login = function () {
        var _this = this;
        this.loading.present();
        var obj = {
            email: this.username,
            password: this.password
        };
        this.api.postRequset(this.url.login, obj).subscribe(function (response_api) {
            // var response_api  = new APIResponse(response); 
            _this.loading.dismiss();
            if (response_api.status === _this.constant.ok) {
                _this.constant.user = JSON.parse(response_api.data).appuser;
                alert("success: " + _this.constant.user.email);
            }
            else {
                alert("Failed: " + response_api.errors[0].code);
            }
        }, function (error) {
            _this.loading.dismiss();
            alert("error" + error);
        });
    };
    LoginPage.prototype.loginFirebase = function () {
        var _this = this;
        debugger;
        if (!this.username) {
            return;
        }
        var credentials = {
            email: this.username,
            password: this.password
        };
        this.firebaseProvider.signInWithEmail(credentials)
            .then(function (user) {
            if (user.user.emailVerified) {
                _this.firebaseProvider.Superuser = user.user;
                var pre_user = user.user;
                if (pre_user) {
                    _this.firebaseProvider.findEmail(pre_user.email).then(function (ref) {
                        _this.uber = ref;
                        ////
                        _this.uber.subscribe(function (res) {
                            console.log(res);
                            var m_user = res;
                            if (m_user.length === 0) {
                                _this.firebaseProvider.isRegistredUser = false;
                                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_0__Register_register__["a" /* RegisterPage */], { "email": pre_user.email });
                            }
                            else {
                                _this.firebaseProvider.username = res[0].Username;
                                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__home_home__["a" /* HomePage */]);
                            }
                        }, function () {
                            _this.firebaseProvider.isRegistredUser = false;
                            return false;
                        });
                    });
                }
                ////////////////
            }
            else {
                alert("Email not varifired.Please check your mailbox");
                _this.firebaseProvider.sendEmailVerificationLink(user.user).then(function (ref) {
                }, function (error) {
                    alert(error.message);
                });
            }
        }, function (error) { alert(error.message); _this.loginError = error.message; });
    };
    LoginPage.prototype.signup = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_0__Register_register__["a" /* RegisterPage */], { "email": "null" });
    };
    LoginPage.prototype.hideShowPassword = function () {
        this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
        this.paIcon = this.paIcon === 'eye-off' ? 'eye' : 'eye-off';
    };
    LoginPage.prototype.forgotPassword = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__forgot_forgot__["a" /* ForgotPage */]);
    };
    LoginPage.prototype.loginWithGooglePlus = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                // alert("succed");
                this.gPlus.login({
                    'scopes': '',
                    'webClientId': __WEBPACK_IMPORTED_MODULE_9__environments_environment__["a" /* environment */].googleWebClientId,
                    'offline': true,
                }).then(function (user) {
                    //save user data on the native storage\
                    alert("succed");
                    // this.nativeStorage.setItem('google_user', {
                    //   name: user.displayName,
                    //   email: user.email,
                    //   picture: user.imageUrl         
                    // }).then(() => {
                    // }, (error) => {
                    //   console.log(error);
                    // })
                }, function (err) {
                    alert("failed" + err);
                });
                return [2 /*return*/];
            });
        });
    };
    // Function for Sigin in With Google using FirebaseAuth
    LoginPage.prototype.loginWithGoogle = function () {
        var _this = this;
        this.firebaseProvider.signInWithGoogle()
            .then(function (user) {
            if (user.user.emailVerified) {
                _this.firebaseProvider.Superuser = user.user;
                var pre_user = user.user;
                if (pre_user) {
                    _this.firebaseProvider.findEmail(pre_user.email).then(function (ref) {
                        _this.uber = ref;
                        ////
                        _this.uber.subscribe(function (res) {
                            console.log(res);
                            var m_user = res;
                            if (m_user.length === 0) {
                                _this.firebaseProvider.isRegistredUser = false;
                                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_0__Register_register__["a" /* RegisterPage */], { "email": pre_user.email });
                            }
                            else {
                                _this.firebaseProvider.username = res[0].Username;
                                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__home_home__["a" /* HomePage */]);
                            }
                        }, function () {
                            _this.firebaseProvider.isRegistredUser = false;
                            return false;
                        });
                    });
                }
            }
            else {
                alert("Email not varifired.Please check your mailbox");
            }
        }, function (error) { return console.log(error.message); });
    };
    LoginPage.prototype.loginWithFacebook = function () {
        var _this = this;
        this.facebook.login(['public_profile', 'user_friends', 'email'])
            .then(function (res) {
            if (res.status === "connected") {
                // this.isLoggedIn = true;
                _this.facebook.api("/" + res.authResponse.userID + "/?fields=id,email,name,picture,gender", ["public_profile"])
                    .then(function (res) {
                    console.log(res);
                    alert("succes" + res);
                    // this.users = res;
                })
                    .catch(function (e) {
                    alert("exception" + e);
                    console.log(e);
                });
            }
            else {
                alert("failed" + res);
                // this.isLoggedIn = false;
            }
        })
            .catch(function (e) { return console.log('Error logging into Facebook', e); });
    };
    LoginPage.prototype.saveData = function () {
        var obj = {
            UserName: "N/A",
            Password: "N/A",
            Name: "N/A",
            Email: this.firebaseProvider.getEmail()
        };
        this.firebaseProvider.addUser(obj);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_8" /* ViewChild */])('email'),
        __metadata("design:type", Object)
    ], LoginPage.prototype, "email", void 0);
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/vaidahi/Desktop/KheloJiyoiOS/khelojiyoiosionic/src/pages/login/login.html"*/'<ion-content class="login auth-page">\n  <div class="login-content">\n    <!-- Logo -->\n    <div padding-horizontal text-center class="animated fadeInDown">\n      <div class="logo"></div>\n    </div>\n    <ion-row>\n    </ion-row>\n    <div>\n      <form #loginForm="ngForm" autocomplete="off">\n        <ion-row>\n          <ion-col>\n            <ion-list inset>\n              <ion-item no-lines class="item-input-block">\n                <ion-icon name="mail" item-start style="font-size:22px;" class="text-grey">\n                </ion-icon>\n                <ion-input text-center name="username" id="loginField" placeholder="play@khelojiyo.com" type="email"\n                  required [(ngModel)]=\'username\' #email></ion-input>\n              </ion-item>\n              <ion-item no-lines class="item-input-block">\n                <ion-icon name="key" item-start style="font-size:22px;" class="text-grey">\n                </ion-icon>\n                <ion-input text-center name="password" id="passwordField" [(ngModel)]=\'password\' [type]="passwordType"\n                  placeholder="********" required></ion-input>\n                <button class="text-grey" ion-button clear item-end (click)=\'hideShowPassword()\'>\n                  <ion-icon [name]="paIcon" style="font-size:22px;"></ion-icon>\n                </button>\n              </ion-item>\n            </ion-list>\n          </ion-col>\n        </ion-row>\n        <ion-row style="margin-left:18px;\n                       margin-right:18px;">\n            <ion-col>\n            <div *ngIf="error" class="alert alert-danger">{{ error }}</div>\n            <button ion-button class="submit-btn" style="padding:22px;border-radius: 5px;" full type="submit" (click)="login()"\n              [disabled]="!loginForm.form.valid">\n              Login\n            </button>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <div class="wrapper-div">\n            <div class="left_item">\n              <button ion-button style="font-size:12px;\n                  font-style: italic;\n                  text-transform: initial;\n                  text-decoration: underline" class="text-white" clear (click)="forgotPassword()">\n                Forgot password?\n              </button>\n            </div>\n            <div class="right_item">\n              <button ion-button style="font-size:12px; \n              font-style: italic;\n              text-transform: initial;\n              text-decoration: underline" class="text-white" clear (click)="signup()">\n                New User? Sign Up\n              </button>\n            </div>\n          </div>\n        </ion-row>\n        <B>\n          <div style="text-align:center;margin:20px;\n                      color: white;font-size:12px;">\n            Or, Sign in using\n          </div>\n        </B>\n        <ion-row class="right-text" style="margin:18px;">\n          <ion-list class="right-text">\n            <div class="wrapper-div">\n              <div class="left_item">\n                <button class="loginBtn loginBtn--facebook" (click)="loginWithFacebook()">\n                  FACEBOOK\n                </button>\n              </div>\n              <div class="right_item">\n                <button class="loginBtn loginBtn--google" (click)="loginWithGoogle()">\n                  &nbsp;GOOGLE&nbsp;\n                </button>\n              </div>\n            </div>\n          </ion-list>\n        </ion-row>\n      </form>\n    </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/vaidahi/Desktop/KheloJiyoiOS/khelojiyoiosionic/src/pages/login/login.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__providers_firebase_firebase__["a" /* FirebaseProvider */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_google_plus__["a" /* GooglePlus */],
            __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["a" /* AngularFireDatabase */],
            __WEBPACK_IMPORTED_MODULE_10__providers_data_service_data_service__["a" /* DataServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_10__providers_data_service_data_service__["b" /* Url */],
            __WEBPACK_IMPORTED_MODULE_11__ionic_native_facebook__["a" /* Facebook */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["f" /* LoadingController */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 166:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return Url; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DataServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(165);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the DataServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var Url = /** @class */ (function () {
    function Url() {
        this.ok = "200";
        this.host = "http://192.168.10.135";
        this.baseUrl = this.host + "/api/account/";
        this.login = this.baseUrl + "login";
        this.register = this.baseUrl + "register";
    }
    return Url;
}());

var DataServiceProvider = /** @class */ (function () {
    function DataServiceProvider(http) {
        this.http = http;
        console.log('Hello DataServiceProvider Provider');
    }
    DataServiceProvider.prototype.postRequset = function (url, param) {
        //var headers = new Headers();
        //  headers.set('Content-type','application/json');
        // return this.http.post(url,param).map((response:any)=>response.json())
        return this.http.post(url, param, { headers: { 'Content-Type': 'application/json' } });
        // return this.http.get('assets/data/products.json')
        //   .map((response:Response)=>response.json());
        // var headers = new Headers();
        //    headers.set('Content-type','application/json');
        //   this.http.post(url ,param,{headers})
        //   .subscribe(res => {          
        //               return res['_body'];
        //   }, (err) => {
        //               return  err;
        //   });
    };
    DataServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], DataServiceProvider);
    return DataServiceProvider;
}());

//# sourceMappingURL=data-service.js.map

/***/ }),

/***/ 204:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 204;

/***/ }),

/***/ 205:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GoogleauthPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the GoogleauthPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var GoogleauthPage = /** @class */ (function () {
    function GoogleauthPage(navCtrl) {
        this.navCtrl = navCtrl;
        this.isLoggedIn = false;
    }
    GoogleauthPage.prototype.login = function () {
    };
    GoogleauthPage.prototype.logout = function () {
    };
    GoogleauthPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-googleauth',template:/*ion-inline-start:"/Users/vaidahi/Desktop/KheloJiyoiOS/khelojiyoiosionic/src/pages/googleauth/googleauth.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Ionic-Master</ion-title>\n    <ion-buttons end>\n      <button ion-button tappable>\n        <ion-icon name="notifications"></ion-icon>\n      </button>\n      <button ion-button tappable>\n        <ion-icon name="cog"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <div *ngIf="isLoggedIn; else loginTemplate">\n    <h1>Welcome, {{displayName}}!</h1>\n    <p>Email: {{email}}<br>\n      Family Name: {{familyName}}<br>\n      Given Name: {{givenName}}<br>\n      User ID: {{userId}}</p>\n    <p><ion-avatar item-left>\n        <img src="{{imageUrl}}">\n       </ion-avatar></p>\n    <p><button ion-button (click)="logout()">Logout From Google</button></p>\n  </div>\n\n  <ng-template #loginTemplate>\n    <h1>Please Login to see your Google Account Information</h1>\n    <p><button ion-button (click)="login()">Login With Google</button></p>\n  </ng-template>\n</ion-content>\n'/*ion-inline-end:"/Users/vaidahi/Desktop/KheloJiyoiOS/khelojiyoiosionic/src/pages/googleauth/googleauth.html"*/,
            providers: []
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */]])
    ], GoogleauthPage);
    return GoogleauthPage;
}());

//# sourceMappingURL=googleauth.js.map

/***/ }),

/***/ 248:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/hometest/hometest.module": [
		532,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 248;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 288:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TeamProfile; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__about_about__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__contact_contact__ = __webpack_require__(160);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var TeamProfile = /** @class */ (function () {
    function TeamProfile(platform, statusBar, splashScreen) {
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_4__about_about__["a" /* AboutPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_5__contact_contact__["a" /* ContactPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    TeamProfile = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/vaidahi/Desktop/KheloJiyoiOS/khelojiyoiosionic/src/pages/teamprofile/teamprofile.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Ionic-Master</ion-title>\n    <ion-buttons end>\n      <button ion-button tappable>\n        <ion-icon name="notifications"></ion-icon>\n      </button>\n      <button ion-button tappable>\n        <ion-icon name="cog"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content padding>\n  <!-- <h3>Ionic Menu Starter</h3>\n  <p>\n    If you get lost, the <a href="http://ionicframework.com/docs/v2">docs</a> will show you the way.\n  </p>\n  <button ion-button secondary menuToggle>Toggle Menu</button> -->\n  <ion-tabs>\n    <ion-tab [root]="tab2Root" tabTitle="About" tabIcon="information-circle"></ion-tab>\n    <ion-tab [root]="tab3Root" tabTitle="Contact" tabIcon="contacts"></ion-tab>\n  </ion-tabs>\n\n</ion-content>\n'/*ion-inline-end:"/Users/vaidahi/Desktop/KheloJiyoiOS/khelojiyoiosionic/src/pages/teamprofile/teamprofile.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], TeamProfile);
    return TeamProfile;
}());

//# sourceMappingURL=teamprofile.js.map

/***/ }),

/***/ 303:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ForgotPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__login_login__ = __webpack_require__(164);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Register_register__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_firebase_firebase__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__home_home__ = __webpack_require__(93);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ForgotPage = /** @class */ (function () {
    function ForgotPage(navCtrl, http, afDatabase, firebaseProvider) {
        this.navCtrl = navCtrl;
        this.http = http;
        this.afDatabase = afDatabase;
        this.firebaseProvider = firebaseProvider;
        this.apiUrl = 'http://72.249.170.12/BluetoothApi/api/Login/Login';
        this.passwordType = 'password';
        this.paIcon = 'eye-off';
        debugger;
        this.user = this.firebaseProvider.getShoppingItems();
    }
    //https://www.djamware.com/post/59924f9080aca768e4d2b12e/ionic-3-consuming-rest-api-using-new-angular-43-httpclient
    ForgotPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        setTimeout(function () {
            _this.email.setFocus();
        }, 500);
    };
    // login(): void {
    //   const obj = {
    //     "UserName":"admin",
    //     "Password":"admin"
    //    }
    // var headers = new Headers();
    //     headers.set('Content-type','application/json');
    //       console.log('doing'+ JSON.stringify(obj));
    //       debugger;
    //        const uri = 'http://72.249.170.12/HRDCServiceOnline/user.svc/UserLogin';
    //        this.http.post(uri,JSON.stringify(obj),{headers})
    //       .subscribe(res => {
    //         console.log("done");
    //         alert('done');
    //       }, (err) => {
    //         console.log(err);
    //       });
    // }
    ForgotPage.prototype.login = function () {
        var _this = this;
        if (!this.username) {
            return;
        }
        this.firebaseProvider.resetPassword(this.username)
            .then(function () {
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_0__login_login__["a" /* LoginPage */]);
            alert("You reset password requset will be sent to your Email, Please chceck your inbox now!");
        }, function (error) { alert(error.message); _this.loginError = error.message; });
    };
    ForgotPage.prototype.signup = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_1__Register_register__["a" /* RegisterPage */], { "email": "null" });
    };
    ForgotPage.prototype.hideShowPassword = function () {
        this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
        this.paIcon = this.paIcon === 'eye-off' ? 'eye' : 'eye-off';
    };
    ForgotPage.prototype.loginWithGoogle = function () {
        var _this = this;
        debugger;
        this.firebaseProvider.signInWithGoogle()
            .then(function (user) {
            if (user.user.emailVerified) {
                _this.firebaseProvider.Superuser = user.user;
                var pre_user = user.user;
                if (pre_user) {
                    _this.firebaseProvider.findEmail(pre_user.email).then(function (ref) {
                        _this.uber = ref;
                        ////
                        _this.uber.subscribe(function (res) {
                            console.log(res);
                            var m_user = res;
                            if (m_user.length === 0) {
                                _this.firebaseProvider.isRegistredUser = false;
                                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_1__Register_register__["a" /* RegisterPage */], { "email": pre_user.email });
                            }
                            else {
                                _this.firebaseProvider.username = res[0].Username;
                                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__home_home__["a" /* HomePage */]);
                            }
                        }, function () {
                            _this.firebaseProvider.isRegistredUser = false;
                            return false;
                        });
                    });
                }
            }
            else {
                alert("Email not varifired.Please check your mailbox");
            }
        }, function (error) { return console.log(error.message); });
    };
    ForgotPage.prototype.finish = function () {
        this.navCtrl.pop();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["_8" /* ViewChild */])('email'),
        __metadata("design:type", Object)
    ], ForgotPage.prototype, "email", void 0);
    ForgotPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["m" /* Component */])({
            selector: 'page-forgot',template:/*ion-inline-start:"/Users/vaidahi/Desktop/KheloJiyoiOS/khelojiyoiosionic/src/pages/forgot/forgot.html"*/'<ion-content padding class="login auth-page">\n  <div class="login-content">\n    <div padding-horizontal text-center>\n      <div class="lock"></div>\n    </div>\n    <!-- Logo -->\n    <div  class="item-input-block" style="padding-right: 16px; padding-bottom: 16px" padding-horizontal text-center>\n      <br />\n      <h2 ion-text class="text-primary text-white ">\n        <strong> Recover Your Password</strong>\n      </h2>\n      <br />\n      <h6 ion-text class=" text-primary text-white ">\n         We will send you a link to reset your password \n      </h6>\n  \n    <div>\n      <form #loginForm="ngForm" autocomplete="off">\n        <ion-row>\n          <ion-col>\n            <ion-item no-lines class="grey-input-block">\n              <ion-icon name="mail" item-start style="font-size:22px;" class="text-white"> </ion-icon>\n              <ion-input text-center name="username" \n                   id="loginField"  \n                   placeholder="play@khelojiyo.com"\n                   type="email"\n                    required [(ngModel)]=\'username\' #email ></ion-input>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col>\n            <div *ngIf="error" class="alert alert-danger">{{ error }}</div>\n            <button ion-button class="submit-btn" full type="submit" (click)="login()"\n              [disabled]="!loginForm.form.valid">Submit Email Id\n            </button>\n          </ion-col>\n        </ion-row>\n        <br />\n        <div style="text-align:center;margin:20px;\n          color: white;font-size:12px;\n          font-size:12px; \n          font-style: italic;\n          text-transform: initial;         \n          text-decoration: underline" (click)="finish()">\n          Back To Login\n         \n        </div>\n      \n            \n\n\n      </form>\n\n    </div>\n  </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/vaidahi/Desktop/KheloJiyoiOS/khelojiyoiosionic/src/pages/forgot/forgot.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_5__providers_firebase_firebase__["a" /* FirebaseProvider */]])
    ], ForgotPage);
    return ForgotPage;
}());

//# sourceMappingURL=forgot.js.map

/***/ }),

/***/ 305:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MapslocationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__ = __webpack_require__(306);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_device__ = __webpack_require__(307);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the MapslocationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MapslocationPage = /** @class */ (function () {
    function MapslocationPage(navCtrl, platform, geolocation, device) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.geolocation = geolocation;
        this.device = device;
        this.markers = [];
        debugger;
        platform.ready().then(function () {
            debugger;
            _this.initMap();
        });
    }
    MapslocationPage.prototype.initMap = function () {
        var _this = this;
        this.geolocation.getCurrentPosition().then(function (resp) {
            var mylocation = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
            debugger;
            _this.map = new google.maps.Map(_this.mapElement.nativeElement, {
                zoom: 18,
                center: mylocation
            });
        });
        var watch = this.geolocation.watchPosition();
        watch.subscribe(function (data) {
            _this.deleteMarkers();
            _this.updateGeolocation(_this.device.uuid, data.coords.latitude, data.coords.longitude);
            var updatelocation = new google.maps.LatLng(data.coords.latitude, data.coords.longitude);
            var image = 'assets/imgs/pin.png';
            _this.addMarker("XDuce", updatelocation, image);
            var latLng2 = new google.maps.LatLng(23.033048, 72.562212);
            _this.addMarker("Municipal Market", latLng2, image);
            _this.setMapOnAll(_this.map);
        });
    };
    MapslocationPage.prototype.addMarker = function (name, location, image) {
        var marker = new google.maps.Marker({
            title: name,
            position: location,
            map: this.map,
            icon: image
        });
        this.markers.push(marker);
        this.addInfoWindowToMarker(marker);
    };
    MapslocationPage.prototype.addInfoWindowToMarker = function (marker) {
        var _this = this;
        var infoWindowContent = '<div id="content"><h1 id="firstHeading" class="firstHeading">' + marker.title + '</h1></div>';
        var infoWindow = new google.maps.InfoWindow({
            content: infoWindowContent
        });
        marker.addListener('click', function () {
            infoWindow.open(_this.map, marker);
        });
    };
    MapslocationPage.prototype.setMapOnAll = function (map) {
        for (var i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(map);
        }
    };
    MapslocationPage.prototype.clearMarkers = function () {
        this.setMapOnAll(null);
    };
    MapslocationPage.prototype.deleteMarkers = function () {
        this.clearMarkers();
        this.markers = [];
    };
    MapslocationPage.prototype.updateGeolocation = function (uuid, lat, lng) {
    };
    MapslocationPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MapslocationPage');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], MapslocationPage.prototype, "mapElement", void 0);
    MapslocationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-mapslocation',template:/*ion-inline-start:"/Users/vaidahi/Desktop/KheloJiyoiOS/khelojiyoiosionic/src/pages/mapslocation/mapslocation.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Ionic-Master</ion-title>\n    <ion-buttons end>\n      <button ion-button tappable>\n        <ion-icon name="notifications"></ion-icon>\n      </button>\n      <button ion-button tappable>\n        <ion-icon name="cog"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <div #map id="map"></div>\n</ion-content>\n'/*ion-inline-end:"/Users/vaidahi/Desktop/KheloJiyoiOS/khelojiyoiosionic/src/pages/mapslocation/mapslocation.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_device__["a" /* Device */]])
    ], MapslocationPage);
    return MapslocationPage;
}());

//# sourceMappingURL=mapslocation.js.map

/***/ }),

/***/ 308:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Model_model__ = __webpack_require__(309);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_debounceTime__ = __webpack_require__(523);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_debounceTime___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_debounceTime__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__ = __webpack_require__(165);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_http__ = __webpack_require__(72);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ListPage = /** @class */ (function () {
    function ListPage(navCtrl, navParams, httpClient, modalCtrl, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.httpClient = httpClient;
        this.modalCtrl = modalCtrl;
        this.http = http;
        this.pageno = 0;
        this.items = [];
        var obj = { UserId: 1, ParentId: 1, Page: 0, PageSize: 10 };
        this.searchControl = new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormControl */]();
        this.apipaggingcall(this.pageno);
        // this.films = this.httpClient.get('https://swapi.co/api/films');
        //  var headers = new Headers();
        //     headers.set('Content-type','application/json');
        //       console.log('doing'+ JSON.stringify(obj));
        //       debugger;
        //        const uri = 'http://72.249.170.12/InsuranceCalicaApi/api/Quotation/GetQuotationListByTLTC';
        //        this.http.post(uri,JSON.stringify(obj),{headers})
        //  // this.films
        //   .subscribe(data => {
        //     debugger;
        //     var json=data;
        //     console.log('my data: ', json);
        //     alert(data.json()['ResponseData'][0].QuotationId);
        //   //   this.Tags=JSON.stringify(data);
        //   //  // console.log('my data: ', this.Tags);
        //   //  var datanew =JSON.parse(this.Tags);
        //   this.items = [];
        //   for (let item of data.json()['ResponseData'])
        //   {
        //     console.log("title",item.QuotationCode);
        //     this.items.push({
        //           title: 'Item ' + item.QuotationCode,
        //           note: 'This is item #' + item.QuotationId
        //         });
        //   }
        //   this.itemtemp =this.items
        //   })
    }
    ListPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.searchControl.valueChanges.debounceTime(100).subscribe(function (search) {
            _this.items = _this.itemtemp;
            _this.setFilteredItems();
        });
    };
    ListPage.prototype.itemTapped = function (event, item) {
        // That's right, we're pushing to ourselves!
        // this.navCtrl.push(ListPage, {
        //   item: item
        // });
        var index = this.items.indexOf(item);
        alert(index);
        this.items.splice(index, 1);
    };
    ListPage.prototype.itemTappededit = function (event, item) {
        // That's right, we're pushing to ourselves!
        // this.navCtrl.push(ListPage, {
        //   item: item
        // });
        var index = this.items.indexOf(item);
        alert(index);
        this.items.splice(index, 1, {
            title: 'Item Dummy',
            note: 'This is item # dummy'
        });
    };
    ListPage.prototype.openModal = function (event, item) {
        var _this = this;
        this.index = this.items.indexOf(item);
        var data = { message: item };
        var modalPage = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_0__Model_model__["a" /* ModelPage */], data);
        modalPage.onDidDismiss(function (data) {
            console.log(data);
            _this.items.splice(_this.index, 1, {
                title: data.title,
                note: data.note
            });
        });
        modalPage.present();
    };
    ListPage.prototype.setFilteredItems = function () {
        var _this = this;
        this.items = this.items.filter(function (item) {
            return item.title.toLowerCase().indexOf(_this.searchTerm.toLowerCase()) > -1;
        });
    };
    ListPage.prototype.doInfinite = function (infiniteScroll) {
        if (this.lastcallpageno != this.pageno) {
            this.apipaggingcall(this.pageno);
            infiniteScroll.complete();
        }
        else {
            infiniteScroll.complete();
            this.ionViewDidLoad();
        }
    };
    ListPage.prototype.apipaggingcall = function (pageno) {
        var _this = this;
        this.lastcallpageno = pageno;
        var obj = { UserId: 1, ParentId: 1, Page: pageno, PageSize: 10 };
        this.searchControl = new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormControl */]();
        // this.films = this.httpClient.get('https://swapi.co/api/films');
        var headers = new __WEBPACK_IMPORTED_MODULE_7__angular_http__["a" /* Headers */]();
        headers.set('Content-type', 'application/json');
        console.log('doing' + JSON.stringify(obj));
        debugger;
        var uri = 'http://72.249.170.12/InsuranceCalicaApi/api/Quotation/GetQuotationListByTLTC';
        this.http.post(uri, JSON.stringify(obj), { headers: headers })
            .subscribe(function (data) {
            debugger;
            var json = data;
            console.log('my data: ', json);
            //   this.Tags=JSON.stringify(data);
            //  // console.log('my data: ', this.Tags);
            //  var datanew =JSON.parse(this.Tags);
            if (data.json()['ResponseData'].length > 0) {
                for (var _i = 0, _a = data.json()['ResponseData']; _i < _a.length; _i++) {
                    var item = _a[_i];
                    console.log("title", item.QuotationCode);
                    _this.items.push({
                        title: 'Item ' + item.QuotationCode,
                        note: 'This is item #' + item.QuotationId
                    });
                }
                _this.itemtemp = _this.items;
                _this.pageno++;
                alert('call');
            }
        });
    };
    ListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-list',template:/*ion-inline-start:"/Users/vaidahi/Desktop/KheloJiyoiOS/khelojiyoiosionic/src/pages/list/list.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Ionic-Master</ion-title>\n    <ion-buttons end>\n      <button ion-button tappable>\n        <ion-icon name="notifications"></ion-icon>\n      </button>\n      <button ion-button tappable>\n        <ion-icon name="cog"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n    <ion-searchbar [(ngModel)]="searchTerm" [formControl]="searchControl" padding></ion-searchbar>\n  <ion-list>\n    <button ion-item *ngFor="let item of items" >\n      <!-- <ion-icon [name]="item.icon" item-start></ion-icon> -->\n      {{item.title}}\n      <div class="item-note" item-end >\n       <!-- {{item.note}} -->\n       <ion-icon ios="ios-create" md="md-create" (click)="openModal($event, item)" padding></ion-icon>\n       <ion-icon ios="ios-trash" md="md-trash" (click)="itemTapped($event, item)"></ion-icon>\n      </div>\n    </button>\n  </ion-list>\n  <div *ngIf="selectedItem" padding>\n    You navigated here from <b>{{selectedItem.title}}</b>\n  </div>\n  <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n      <ion-infinite-scroll-content></ion-infinite-scroll-content>\n    </ion-infinite-scroll>\n</ion-content>\n'/*ion-inline-end:"/Users/vaidahi/Desktop/KheloJiyoiOS/khelojiyoiosionic/src/pages/list/list.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* ModalController */], __WEBPACK_IMPORTED_MODULE_7__angular_http__["b" /* Http */]])
    ], ListPage);
    return ListPage;
}());

//# sourceMappingURL=list.js.map

/***/ }),

/***/ 309:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModelPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ModelPage = /** @class */ (function () {
    function ModelPage(viewCtrl, navParams) {
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
    }
    ModelPage.prototype.ionViewDidLoad = function () {
        console.log(this.navParams.get('message'));
        this.title = this.navParams.get('message').title;
        this.note = this.navParams.get('message').note;
    };
    ModelPage.prototype.closeModal = function () {
        this.viewCtrl.dismiss({
            title: this.title,
            note: this.note
        });
    };
    ModelPage.prototype.updatesubmit = function () {
        this.viewCtrl.dismiss({
            title: this.title,
            note: this.note
        });
    };
    ModelPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-model',template:/*ion-inline-start:"/Users/vaidahi/Desktop/KheloJiyoiOS/khelojiyoiosionic/src/pages/Model/model.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>ModalPage</ion-title>\n\n    <ion-buttons end>\n\n    <button ion-button (click)="closeModal()">Close</button>\n\n    </ion-buttons>\n\n</ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n\n  <ion-row>\n\n  </ion-row>\n\n  <div >\n\n    <form #modelForm="ngForm" (ngSubmit)="updatesubmit()" autocomplete="off">\n\n      <ion-row>\n\n        <ion-col>\n\n          <ion-list inset>\n\n            <ion-item>\n\n              <ion-input placeholder="Title" name="title" id="titleField"\n\n                         type="text" required [(ngModel)]="title" ></ion-input>\n\n            </ion-item>\n\n            <ion-item>\n\n              <ion-input placeholder="Note" name="note" id="noteField"\n\n                         type="text" required [(ngModel)]="note"></ion-input>\n\n            </ion-item>\n\n          </ion-list>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row>\n\n        <ion-col>\n\n          <button ion-button class="submit-btn" full type="submit"\n\n                  [disabled]="!modelForm.form.valid">Update\n\n          </button>\n\n        </ion-col>\n\n      </ion-row>\n\n    </form>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"/Users/vaidahi/Desktop/KheloJiyoiOS/khelojiyoiosionic/src/pages/Model/model.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["l" /* ViewController */], __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["j" /* NavParams */]])
    ], ModelPage);
    return ModelPage;
}());

//# sourceMappingURL=model.js.map

/***/ }),

/***/ 310:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FacebookloginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_facebook__ = __webpack_require__(168);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FacebookloginPage = /** @class */ (function () {
    function FacebookloginPage(fb) {
        var _this = this;
        this.fb = fb;
        this.isLoggedIn = false;
        fb.getLoginStatus()
            .then(function (res) {
            console.log(res.status);
            if (res.status === "connect") {
                _this.isLoggedIn = true;
            }
            else {
                _this.isLoggedIn = false;
            }
        })
            .catch(function (e) { return console.log(e); });
    }
    FacebookloginPage.prototype.login = function () {
        var _this = this;
        this.fb.login(['public_profile', 'user_friends', 'email'])
            .then(function (res) {
            if (res.status === "connected") {
                _this.isLoggedIn = true;
                _this.getUserDetail(res.authResponse.userID);
            }
            else {
                _this.isLoggedIn = false;
            }
        })
            .catch(function (e) { return console.log('Error logging into Facebook', e); });
    };
    FacebookloginPage.prototype.logout = function () {
        var _this = this;
        this.fb.logout()
            .then(function (res) { return _this.isLoggedIn = false; })
            .catch(function (e) { return console.log('Error logout from Facebook', e); });
    };
    FacebookloginPage.prototype.getUserDetail = function (userid) {
        var _this = this;
        this.fb.api("/" + userid + "/?fields=id,email,name,picture,gender", ["public_profile"])
            .then(function (res) {
            console.log(res);
            _this.users = res;
        })
            .catch(function (e) {
            console.log(e);
        });
    };
    FacebookloginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-facebooklogin',template:/*ion-inline-start:"/Users/vaidahi/Desktop/KheloJiyoiOS/khelojiyoiosionic/src/pages/facebooklogin/facebooklogin.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Ionic-Master</ion-title>\n    <ion-buttons end>\n      <button ion-button tappable>\n        <ion-icon name="notifications"></ion-icon>\n      </button>\n      <button ion-button tappable>\n        <ion-icon name="cog"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content padding>\n  <div *ngIf="isLoggedIn; else facebookLogin">\n    <h2>Hi, {{users.name}} ({{users.email}})</h2>\n    <p>\n      Gender: {{users.gender}}\n    </p>\n    <p>\n      <img src="{{users.picture.data.url}}" width="100" alt="{{users.name}}" />\n    </p>\n    <p>\n      <button ion-button icon-right (click)="logout()">\n        Logout\n      </button>\n    </p>\n  </div>\n  <ng-template #facebookLogin>\n    <button ion-button icon-right (click)="login()">\n      Login with\n      <ion-icon name="logo-facebook"></ion-icon>\n    </button>\n  </ng-template>\n</ion-content>\n'/*ion-inline-end:"/Users/vaidahi/Desktop/KheloJiyoiOS/khelojiyoiosionic/src/pages/facebooklogin/facebooklogin.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_facebook__["a" /* Facebook */]])
    ], FacebookloginPage);
    return FacebookloginPage;
}());

//# sourceMappingURL=facebooklogin.js.map

/***/ }),

/***/ 311:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpinnerloaderPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SpinnerloaderPage = /** @class */ (function () {
    function SpinnerloaderPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    SpinnerloaderPage.prototype.ionViewDidLoad = function () {
        debugger;
        console.log('SpinnerloaderPage');
        // let loading;
        // loading = this.loadingCtrl.create({ content: "Logging in ,please wait..." });
        // loading.present();
    };
    SpinnerloaderPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-spinnerloader',template:/*ion-inline-start:"/Users/vaidahi/Desktop/KheloJiyoiOS/khelojiyoiosionic/src/pages/spinnerloader/spinnerloader.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Ionic-Master</ion-title>\n    <ion-buttons end>\n      <button ion-button tappable>\n        <ion-icon name="notifications"></ion-icon>\n      </button>\n      <button ion-button tappable>\n        <ion-icon name="cog"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content fullscreen>\n  <ion-spinner name="dots" class="custom custom-dot svg"></ion-spinner>\n  <ion-card class="custombutton">\n  <button type="submit" >\n    <ion-spinner name="bubbles" class="custom custom-dot svg"></ion-spinner> Click me!\n    </button>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"/Users/vaidahi/Desktop/KheloJiyoiOS/khelojiyoiosionic/src/pages/spinnerloader/spinnerloader.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], SpinnerloaderPage);
    return SpinnerloaderPage;
}());

//# sourceMappingURL=spinnerloader.js.map

/***/ }),

/***/ 312:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpeechtotextPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_speech_recognition__ = __webpack_require__(313);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_text_to_speech__ = __webpack_require__(314);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var SpeechtotextPage = /** @class */ (function () {
    function SpeechtotextPage(navCtrl, navParams, speech, zone, tts) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.speech = speech;
        this.zone = zone;
        this.tts = tts;
        this.isListening = false;
        this.isgetting = false;
    }
    SpeechtotextPage.prototype.hasPermission = function () {
        return __awaiter(this, void 0, void 0, function () {
            var permission, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.speech.hasPermission()];
                    case 1:
                        permission = _a.sent();
                        console.log(permission);
                        return [2 /*return*/, permission];
                    case 2:
                        e_1 = _a.sent();
                        console.log(e_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    SpeechtotextPage.prototype.getPermission = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                try {
                    this.speech.requestPermission()
                        .then(function () { return _this.isgetting = true; }, function () { return console.log('Denied'); });
                }
                catch (e) {
                    console.log(e);
                }
                return [2 /*return*/];
            });
        });
    };
    SpeechtotextPage.prototype.listen = function () {
        this.getPermission();
        console.log('listen action triggered');
        // if (this.isListening) {
        //   this.speech.stopListening();
        //   this.toggleListenMode();
        //   return
        // }
        // this.toggleListenMode();
        // let _this = this;
        // this.speech.startListening()
        //   .subscribe(matches => {
        //     _this.zone.run(() => {
        //       _this.matches = matches;
        //     })
        //   }, error => console.error(error));
    };
    SpeechtotextPage.prototype.startlistning = function () {
        var _this = this;
        this.speech.startListening()
            .subscribe(function (matches) {
            _this.zone.run(function () {
                _this.matches = matches;
            });
        }, function (error) { return console.error(error); });
    };
    SpeechtotextPage.prototype.hearlisten = function () {
        this.tts.speak('Hello kiran')
            .then(function () { return console.log('Success'); })
            .catch(function (reason) { return console.log(reason); });
    };
    SpeechtotextPage.prototype.toggleListenMode = function () {
        this.isListening = this.isListening ? false : true;
        console.log('listening mode is now : ' + this.isListening);
    };
    SpeechtotextPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-speechtotext',template:/*ion-inline-start:"/Users/vaidahi/Desktop/KheloJiyoiOS/khelojiyoiosionic/src/pages/speechtotext/speechtotext.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Ionic-Master</ion-title>\n    <ion-buttons end>\n      <button ion-button tappable>\n        <ion-icon name="notifications"></ion-icon>\n      </button>\n      <button ion-button tappable>\n        <ion-icon name="cog"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <p>Is listening ? : {{ isListening }}</p>\n  <button ion-button (click)="getPermission()">Get Permission</button>\n  <button ion-button (click)="listen()">\n    <div>\n      <ion-icon name="mic">\n        <label>Speak to me</label>\n      </ion-icon>\n    </div>\n  </button>\n  <button ion-button (click)="hearlisten()">\n    <div>\n      <ion-icon name="txtmic">\n        <label>text to speech</label>\n      </ion-icon>\n    </div>\n  </button>\n  <ion-card>\n    <ion-card-content *ngFor="let match of matches">\n      <p>{{ match }}</p>\n    </ion-card-content>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"/Users/vaidahi/Desktop/KheloJiyoiOS/khelojiyoiosionic/src/pages/speechtotext/speechtotext.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_speech_recognition__["a" /* SpeechRecognition */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_text_to_speech__["a" /* TextToSpeech */]])
    ], SpeechtotextPage);
    return SpeechtotextPage;
}());

//# sourceMappingURL=speechtotext.js.map

/***/ }),

/***/ 315:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CameraPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__ = __webpack_require__(316);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_photo_library__ = __webpack_require__(317);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};






var CameraPage = /** @class */ (function () {
    function CameraPage(navCtrl, camera, alertCtrl, photoLibrary, document) {
        this.navCtrl = navCtrl;
        this.camera = camera;
        this.alertCtrl = alertCtrl;
        this.photoLibrary = photoLibrary;
        this.document = document;
        this.filter = 1;
        this.filters = {
            blur: { name: 'blur', max: '10', min: '0', value: 0, map: "px" },
            opacity: { name: 'opacity', max: 100, min: 1, value: '100', map: "%" },
            grayscale: { name: 'grayscale', max: 100, min: 0, value: 0, map: "%" },
            brightness: { name: 'brightness', max: 500, min: 25, value: 100, map: "%" },
            sepia: { name: 'sepia', max: '100', min: '0', value: '0', map: "%" }
        };
        this.options = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            allowEdit: true,
            mediaType: this.camera.MediaType.PICTURE
        };
    }
    CameraPage.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.image = new Image();
        this.image.src = 'assets/camera.jpg';
        this.canvasContext = this.getCanvas();
        this.image.onload = function () { return _this.canvasContext.drawImage(_this.image, 0, 0, 400, 400); };
    };
    CameraPage.prototype.getSnap = function () {
        var _this = this;
        this.camera.getPicture(this.options).then(function (imageData) {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64:
            var base64Image = 'data:image/jpeg;base64,' + imageData;
            //console.log(imageData);
            // this.showAlert("",imageData);
            _this.image.src = base64Image;
        }, function (err) {
            // Handle error
            _this.showAlert('Error', 'failed to click image');
        });
    };
    CameraPage.prototype.showAlert = function (title, message) {
        var alert = this.alertCtrl.create({
            title: title || "Alert",
            subTitle: message,
            buttons: ['OK']
        });
        alert.present();
    };
    CameraPage.prototype.applyFilterFromJs = function (filterProp, event) {
        switch (filterProp.name) {
            case "blur":
                this.filters.blur.value = event.value;
                break;
            case "opacity":
                this.filters.opacity.value = event.value;
                break;
            case "grayscale":
                this.filters.grayscale.value = event.value;
                break;
            case "brightness":
                this.filters.brightness.value = event.value;
                break;
            case "sepia":
                this.filters.sepia.value = event.value;
                break;
            default:
                break;
        }
        this.canvasContext.filter = "\n    blur(" + this.filters.blur.value + this.filters.blur.map + ")\n    opacity(" + this.filters.opacity.value + this.filters.opacity.map + ")\n    grayscale(" + this.filters.grayscale.value + this.filters.grayscale.map + ")\n    brightness(" + this.filters.brightness.value + this.filters.brightness.map + ")\n    sepia(" + this.filters.sepia.value + this.filters.sepia.map + ")\n  ";
        this.canvasContext.drawImage(this.image, 0, 0, 400, 800);
    };
    CameraPage.prototype.getCanvas = function () {
        var canvas = this.document.getElementById('canvas');
        var context = canvas.getContext('2d');
        return context;
    };
    CameraPage.prototype.save = function () {
        var album = 'MyAppName';
        var image = this.canvasContext.toDataURL();
        this.photoLibrary.saveImage(image, album);
    };
    CameraPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-camera',template:/*ion-inline-start:"/Users/vaidahi/Desktop/KheloJiyoiOS/khelojiyoiosionic/src/pages/camera/camera.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Camera\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <button ion-button full color="primary" (click)="getSnap()">Take Snap</button>\n    <button ion-button full  (click)="save()">Save</button>\n    <div class="center">\n    <canvas id="canvas"></canvas>\n    </div>\n    <ion-list  no-lines>\n    <div *ngFor="let filterProp of filters|keys">\n      <h3>{{filterProp.value.name | uppercase}}</h3>\n  <ion-range [min]="filterProp.value.min" [(ngModel)]="filterProp.value.value" [max]="filterProp.value.max"\n      (ionChange)="applyFilterFromJs(filterProp.value,$event)">\n       <ion-label range-left>{{filterProp.value.min}}</ion-label>\n      <ion-label range-right>{{filterProp.value.max}}</ion-label>\n  </ion-range>\n</div>\n</ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/vaidahi/Desktop/KheloJiyoiOS/khelojiyoiosionic/src/pages/camera/camera.html"*/
        }),
        __param(4, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["z" /* Inject */])(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["b" /* DOCUMENT */])),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_photo_library__["a" /* PhotoLibrary */], Object])
    ], CameraPage);
    return CameraPage;
}());

//# sourceMappingURL=camera.js.map

/***/ }),

/***/ 319:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(320);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(442);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);



Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["_14" /* enableProdMode */])();
Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 442:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export firebaseConfig */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__pages_googleauth_googleauth__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pages_teamprofile_teamprofile__ = __webpack_require__(288);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__(490);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_home_home__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_contact_contact__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_about_about__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_tabs_tabs__ = __webpack_require__(526);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_login_login__ = __webpack_require__(164);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_Register_register__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__angular_http__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_common_http__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_Model_model__ = __webpack_require__(309);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_mapslocation_mapslocation__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_geolocation__ = __webpack_require__(306);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ionic_native_device__ = __webpack_require__(307);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__ionic_native_facebook__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_facebooklogin_facebooklogin__ = __webpack_require__(310);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_spinnerloader_spinnerloader__ = __webpack_require__(311);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__providers_data_service_data_service__ = __webpack_require__(166);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_speechtotext_speechtotext__ = __webpack_require__(312);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_speech_recognition__ = __webpack_require__(313);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ionic_native_text_to_speech__ = __webpack_require__(314);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_camera_camera__ = __webpack_require__(315);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__ionic_native_camera__ = __webpack_require__(316);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__ionic_native_photo_library__ = __webpack_require__(317);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pipes_keys_pipe__ = __webpack_require__(527);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_list_list__ = __webpack_require__(308);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31_angularfire2__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32_angularfire2_database__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__providers_firebase_firebase__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34_angularfire2_firestore__ = __webpack_require__(528);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35_angularfire2_auth__ = __webpack_require__(299);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pages_forgot_forgot__ = __webpack_require__(303);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__ionic_native_google_plus__ = __webpack_require__(304);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__ionic_native_splash_screen__ = __webpack_require__(92);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

// import { HttpClient } from '@angular/common/http';






























// Import the AF2 Module








//import { AlertController } from 'ionic-angular';
// AF2 Settings
var firebaseConfig = {
    apiKey: "AIzaSyD6QoZtgVsMJnOlV2-5BS7R_OSJyTIeKr8",
    authDomain: "khelojiyoionic.firebaseapp.com",
    databaseURL: "https://khelojiyoionic.firebaseio.com",
    projectId: "khelojiyoionic",
    storageBucket: "khelojiyoionic.appspot.com",
    messagingSenderId: "899015697986"
};
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* KheloJiyo */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_list_list__["a" /* ListPage */], __WEBPACK_IMPORTED_MODULE_10__pages_tabs_tabs__["a" /* TabsPage */], __WEBPACK_IMPORTED_MODULE_9__pages_about_about__["a" /* AboutPage */], __WEBPACK_IMPORTED_MODULE_8__pages_contact_contact__["a" /* ContactPage */], __WEBPACK_IMPORTED_MODULE_1__pages_teamprofile_teamprofile__["a" /* TeamProfile */], __WEBPACK_IMPORTED_MODULE_11__pages_login_login__["a" /* LoginPage */], __WEBPACK_IMPORTED_MODULE_12__pages_Register_register__["a" /* RegisterPage */], __WEBPACK_IMPORTED_MODULE_15__pages_Model_model__["a" /* ModelPage */], __WEBPACK_IMPORTED_MODULE_0__pages_googleauth_googleauth__["a" /* GoogleauthPage */], __WEBPACK_IMPORTED_MODULE_16__pages_mapslocation_mapslocation__["a" /* MapslocationPage */], __WEBPACK_IMPORTED_MODULE_20__pages_facebooklogin_facebooklogin__["a" /* FacebookloginPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_spinnerloader_spinnerloader__["a" /* SpinnerloaderPage */], __WEBPACK_IMPORTED_MODULE_23__pages_speechtotext_speechtotext__["a" /* SpeechtotextPage */], __WEBPACK_IMPORTED_MODULE_26__pages_camera_camera__["a" /* CameraPage */], __WEBPACK_IMPORTED_MODULE_29__pipes_keys_pipe__["a" /* KeysPipe */], __WEBPACK_IMPORTED_MODULE_36__pages_forgot_forgot__["a" /* ForgotPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_13__angular_http__["c" /* HttpModule */], __WEBPACK_IMPORTED_MODULE_14__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* KheloJiyo */], {}, {
                    links: [
                        { loadChildren: '../pages/hometest/hometest.module#HomePageModule', name: 'HomePagetest', segment: 'hometest', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_32_angularfire2_database__["b" /* AngularFireDatabaseModule */],
                __WEBPACK_IMPORTED_MODULE_31_angularfire2__["a" /* AngularFireModule */].initializeApp(firebaseConfig),
                __WEBPACK_IMPORTED_MODULE_34_angularfire2_firestore__["a" /* AngularFirestoreModule */],
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* KheloJiyo */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_list_list__["a" /* ListPage */], __WEBPACK_IMPORTED_MODULE_10__pages_tabs_tabs__["a" /* TabsPage */], __WEBPACK_IMPORTED_MODULE_9__pages_about_about__["a" /* AboutPage */], __WEBPACK_IMPORTED_MODULE_8__pages_contact_contact__["a" /* ContactPage */], __WEBPACK_IMPORTED_MODULE_1__pages_teamprofile_teamprofile__["a" /* TeamProfile */], __WEBPACK_IMPORTED_MODULE_11__pages_login_login__["a" /* LoginPage */], __WEBPACK_IMPORTED_MODULE_12__pages_Register_register__["a" /* RegisterPage */], __WEBPACK_IMPORTED_MODULE_15__pages_Model_model__["a" /* ModelPage */], __WEBPACK_IMPORTED_MODULE_0__pages_googleauth_googleauth__["a" /* GoogleauthPage */], __WEBPACK_IMPORTED_MODULE_16__pages_mapslocation_mapslocation__["a" /* MapslocationPage */], __WEBPACK_IMPORTED_MODULE_20__pages_facebooklogin_facebooklogin__["a" /* FacebookloginPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_spinnerloader_spinnerloader__["a" /* SpinnerloaderPage */], __WEBPACK_IMPORTED_MODULE_23__pages_speechtotext_speechtotext__["a" /* SpeechtotextPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_camera_camera__["a" /* CameraPage */], __WEBPACK_IMPORTED_MODULE_36__pages_forgot_forgot__["a" /* ForgotPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_38__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_10__pages_tabs_tabs__["a" /* TabsPage */], __WEBPACK_IMPORTED_MODULE_9__pages_about_about__["a" /* AboutPage */], __WEBPACK_IMPORTED_MODULE_8__pages_contact_contact__["a" /* ContactPage */], __WEBPACK_IMPORTED_MODULE_1__pages_teamprofile_teamprofile__["a" /* TeamProfile */], __WEBPACK_IMPORTED_MODULE_11__pages_login_login__["a" /* LoginPage */], __WEBPACK_IMPORTED_MODULE_12__pages_Register_register__["a" /* RegisterPage */], __WEBPACK_IMPORTED_MODULE_15__pages_Model_model__["a" /* ModelPage */], __WEBPACK_IMPORTED_MODULE_37__ionic_native_google_plus__["a" /* GooglePlus */], __WEBPACK_IMPORTED_MODULE_16__pages_mapslocation_mapslocation__["a" /* MapslocationPage */], __WEBPACK_IMPORTED_MODULE_20__pages_facebooklogin_facebooklogin__["a" /* FacebookloginPage */],
                __WEBPACK_IMPORTED_MODULE_22__providers_data_service_data_service__["a" /* DataServiceProvider */], __WEBPACK_IMPORTED_MODULE_24__ionic_native_speech_recognition__["a" /* SpeechRecognition */], __WEBPACK_IMPORTED_MODULE_25__ionic_native_text_to_speech__["a" /* TextToSpeech */],
                { provide: __WEBPACK_IMPORTED_MODULE_3__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["c" /* IonicErrorHandler */] }, __WEBPACK_IMPORTED_MODULE_17__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_18__ionic_native_device__["a" /* Device */], __WEBPACK_IMPORTED_MODULE_19__ionic_native_facebook__["a" /* Facebook */], __WEBPACK_IMPORTED_MODULE_27__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_28__ionic_native_photo_library__["a" /* PhotoLibrary */], __WEBPACK_IMPORTED_MODULE_26__pages_camera_camera__["a" /* CameraPage */], __WEBPACK_IMPORTED_MODULE_36__pages_forgot_forgot__["a" /* ForgotPage */],
                __WEBPACK_IMPORTED_MODULE_33__providers_firebase_firebase__["a" /* FirebaseProvider */], __WEBPACK_IMPORTED_MODULE_22__providers_data_service_data_service__["b" /* Url */],
                __WEBPACK_IMPORTED_MODULE_35_angularfire2_auth__["a" /* AngularFireAuth */]
            ],
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 490:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return KheloJiyo; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__pages_home_home__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pages_login_login__ = __webpack_require__(164);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_firebase_firebase__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_mapslocation_mapslocation__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_googleauth_googleauth__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_teamprofile_teamprofile__ = __webpack_require__(288);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_status_bar__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_splash_screen__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_list_list__ = __webpack_require__(308);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_Register_register__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_facebooklogin_facebooklogin__ = __webpack_require__(310);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_spinnerloader_spinnerloader__ = __webpack_require__(311);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_speechtotext_speechtotext__ = __webpack_require__(312);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_camera_camera__ = __webpack_require__(315);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











// import { TabsPage } from '../pages/tabs/tabs';





var KheloJiyo = /** @class */ (function () {
    function KheloJiyo(platform, statusBar, splashScreen, firebaseProvider) {
        this.firebaseProvider = firebaseProvider;
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            setTimeout(function () {
                splashScreen.hide();
            }, 800);
            statusBar.styleDefault();
        });
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Home', component: __WEBPACK_IMPORTED_MODULE_0__pages_home_home__["a" /* HomePage */] },
            { title: 'Teams', component: __WEBPACK_IMPORTED_MODULE_5__pages_teamprofile_teamprofile__["a" /* TeamProfile */] },
            { title: 'Login', component: __WEBPACK_IMPORTED_MODULE_1__pages_login_login__["a" /* LoginPage */] },
            { title: 'Register', component: __WEBPACK_IMPORTED_MODULE_11__pages_Register_register__["a" /* RegisterPage */] },
            { title: 'List', component: __WEBPACK_IMPORTED_MODULE_10__pages_list_list__["a" /* ListPage */] },
            { title: 'Google Sign in', component: __WEBPACK_IMPORTED_MODULE_4__pages_googleauth_googleauth__["a" /* GoogleauthPage */] },
            { title: 'MapLocation', component: __WEBPACK_IMPORTED_MODULE_3__pages_mapslocation_mapslocation__["a" /* MapslocationPage */] },
            { title: 'facebooklogin', component: __WEBPACK_IMPORTED_MODULE_12__pages_facebooklogin_facebooklogin__["a" /* FacebookloginPage */] },
            { title: 'SpinnerLoader', component: __WEBPACK_IMPORTED_MODULE_13__pages_spinnerloader_spinnerloader__["a" /* SpinnerloaderPage */] },
            { title: 'Speechtotext', component: __WEBPACK_IMPORTED_MODULE_14__pages_speechtotext_speechtotext__["a" /* SpeechtotextPage */] },
            { title: 'Camera & Filter', component: __WEBPACK_IMPORTED_MODULE_15__pages_camera_camera__["a" /* CameraPage */] },
        ];
        this.viewDidLoad();
    }
    KheloJiyo.prototype.viewDidLoad = function () {
        var _this = this;
        debugger;
        this.firebaseProvider.afAuth.authState.subscribe(function (user) {
            if (user && user.emailVerified) {
                _this.SuperUser = user;
                _this.firebaseProvider.setUsername();
                _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_1__pages_login_login__["a" /* LoginPage */]);
                //this.nav.setRoot(HomePage);
            }
            else {
                _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_1__pages_login_login__["a" /* LoginPage */]);
            }
        }, function (error) {
            _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_1__pages_login_login__["a" /* LoginPage */]);
        });
    };
    KheloJiyo.prototype.openPage = function (page) {
        this.nav.setRoot(page.component);
    };
    // openPage() {
    //   debugger;
    //   this.firebaseProvider.afAuth.authState.subscribe( user => {
    //     if (user && user.emailVerified) {
    //        this.firebaseProvider.setFreshUser(user);
    //        this.rootPage=HomePage
    //        this.SuperUser=user;
    //       (this.uber = this.firebaseProvider.findEmail(this.SuperUser.email.toLowerCase()))
    //       .subscribe ((res: User[]) => {
    //         console.log(res);
    //         this.username = res[0].Username;
    //       });
    //       // this.nav.setRoot(this.rootPage);
    //     } else {
    //       this.rootPage=LoginPage;
    //       this.nav.setRoot(this.rootPage);
    //     }
    //   },
    //  () => {
    //   this.rootPage=LoginPage;
    //   this.nav.setRoot(this.rootPage);
    //   console.log("logout error");
    //  }
    //  );
    // }
    KheloJiyo.prototype.logout = function () {
        var _this = this;
        debugger;
        this.firebaseProvider.signOut().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            console.log("logout");
            _this.firebaseProvider.username = "";
            _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_1__pages_login_login__["a" /* LoginPage */]);
        }, function (error) {
            console.log("logout error");
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_6__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_7_ionic_angular__["h" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_7_ionic_angular__["h" /* Nav */])
    ], KheloJiyo.prototype, "nav", void 0);
    KheloJiyo = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_6__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/vaidahi/Desktop/KheloJiyoiOS/khelojiyoiosionic/src/app/app.html"*/'<ion-menu [content]="content">\n  <ion-header>\n      <!-- <ion-toolbar class="user-profile" [class.hide]="firebaseProvider.username.length==0"> -->\n   <ion-toolbar class="user-profile">\n  \n        <ion-grid>\n            <ion-row>\n              <ion-col col-4>\n                  <div class="user-avatar">\n                    <img src="../assets/imgs/avatar.jpeg">\n                  </div>\n              </ion-col>\n              <ion-col padding-top col-8>\n                <h2 ion-text class="no-margin bold text-white">\n                 {{firebaseProvider.username}}\n                </h2>\n                <span ion-text color="light">Customer</span>\n              </ion-col>\n            </ion-row>\n            <ion-row no-padding class="other-data">\n              <ion-col no-padding class="column">\n                <button ion-button icon-left small full color="light" menuClose disabled>\n                  <ion-icon name="contact"></ion-icon>\n                  Edit Profile\n                </button>\n              </ion-col>\n              <ion-col no-padding class="column">\n                <button ion-button icon-left small full color="light" menuClose (click)="logout()">\n                  <ion-icon name="log-out"></ion-icon>\n                  Log Out\n                </button>\n              </ion-col>\n            </ion-row>\n\n          </ion-grid>\n\n        </ion-toolbar>\n\n    <!-- <ion-toolbar>\n      <ion-title>Menu</ion-title>\n    </ion-toolbar> -->\n  </ion-header>\n\n  <ion-content>\n    <ion-list>\n      <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n        {{p.title}}\n      </button>\n    </ion-list>\n  </ion-content>\n\n</ion-menu>\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>\n'/*ion-inline-end:"/Users/vaidahi/Desktop/KheloJiyoiOS/khelojiyoiosionic/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_7_ionic_angular__["k" /* Platform */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_9__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_2__providers_firebase_firebase__["a" /* FirebaseProvider */]])
    ], KheloJiyo);
    return KheloJiyo;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 522:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false,
    googleWebClientId: "899015697986-qrr1uenmppda76bl83b76uufpt8a4jks.apps.googleusercontent.com"
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ 526:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__about_about__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contact_contact__ = __webpack_require__(160);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TabsPage = /** @class */ (function () {
    function TabsPage() {
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_1__about_about__["a" /* AboutPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_2__contact_contact__["a" /* ContactPage */];
    }
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/vaidahi/Desktop/KheloJiyoiOS/khelojiyoiosionic/src/pages/tabs/tabs.html"*/'<!-- <ion-tabs>\n\n  <ion-tab [root]="tab2Root" tabTitle="About" tabIcon="information-circle"></ion-tab>\n\n  <ion-tab [root]="tab3Root" tabTitle="Contact" tabIcon="contacts"></ion-tab>\n\n</ion-tabs> -->\n\n'/*ion-inline-end:"/Users/vaidahi/Desktop/KheloJiyoiOS/khelojiyoiosionic/src/pages/tabs/tabs.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 527:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return KeysPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var KeysPipe = /** @class */ (function () {
    function KeysPipe() {
    }
    KeysPipe.prototype.transform = function (value, args) {
        var keys = [];
        for (var key in value) {
            keys.push({ key: key, value: value[key] });
        }
        return keys;
    };
    KeysPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Pipe */])({ name: 'keys' })
    ], KeysPipe);
    return KeysPipe;
}());

//# sourceMappingURL=keys.pipe.js.map

/***/ }),

/***/ 54:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FirebaseProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_database__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__ = __webpack_require__(299);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase__ = __webpack_require__(508);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_firebase__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




/*
  Generated class for the FirebaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var FirebaseProvider = /** @class */ (function () {
    function FirebaseProvider(afd, afAuth) {
        var _this = this;
        this.afd = afd;
        this.afAuth = afAuth;
        this.username = "";
        this.isRegistredUser = false;
        afAuth.authState.subscribe(function (user) {
            _this.Superuser = user;
        });
        debugger;
        this.db_users = this.afd.list('/User').valueChanges();
        //console.log(""+this.usert.count);
    }
    FirebaseProvider.prototype.getShoppingItems = function () {
        return this.db_users;
    };
    FirebaseProvider.prototype.findEmail = function (email) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, (this.main_users = this.afd.list('/User', function (ref) { return ref.orderByChild('Email').equalTo(email); }).valueChanges())];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    FirebaseProvider.prototype.addUser = function (name) {
        debugger;
        return this.afd.list('/User/').push(name);
    };
    FirebaseProvider.prototype.removeItem = function (id) {
        this.afd.list('/User/').remove(id);
    };
    FirebaseProvider.prototype.signInWithEmail = function (credentials) {
        console.log('Sign in with email');
        return this.afAuth.auth.signInWithEmailAndPassword(credentials.email, credentials.password);
    };
    FirebaseProvider.prototype.signUp = function (credentials) {
        return this.afAuth.auth.createUserWithEmailAndPassword(credentials.Email, credentials.password);
    };
    FirebaseProvider.prototype.signOut = function () {
        this.Superuser = null;
        return this.afAuth.auth.signOut();
    };
    FirebaseProvider.prototype.getEmail = function () {
        return this.Superuser && this.Superuser.email;
    };
    FirebaseProvider.prototype.getFreshUser = function () {
        var _this = this;
        this.afAuth.authState.subscribe(function (user) {
            if (user) {
                _this.Superuser = user;
                return _this.Superuser;
            }
            else {
                return null;
            }
        }, function () {
            return null;
        });
    };
    FirebaseProvider.prototype.getUser = function () {
        return this.Superuser;
    };
    FirebaseProvider.prototype.setUsername = function () {
        var _this = this;
        if (this.Superuser) {
            this.findEmail(this.Superuser.email).then(function (ref) {
                var uber = ref;
                ////
                uber.subscribe(function (res) {
                    console.log(res);
                    var m_user = res;
                    if (m_user.length !== 0) {
                        _this.username = res[0].Username;
                        _this.isRegistredUser = true;
                    }
                }, function () {
                    _this.isRegistredUser = true;
                });
            });
        }
    };
    FirebaseProvider.prototype.signInWithGoogle = function () {
        console.log('Sign in with google');
        return this.oauthSignIn(new __WEBPACK_IMPORTED_MODULE_3_firebase__["auth"].GoogleAuthProvider());
    };
    FirebaseProvider.prototype.oauthSignIn = function (provider) {
        var _this = this;
        if (!window.cordova) {
            return this.afAuth.auth.signInWithPopup(provider);
        }
        else {
            return this.afAuth.auth.signInWithRedirect(provider)
                .then(function () {
                return _this.afAuth.auth.getRedirectResult().then(function (result) {
                    // This gives you a Google Access Token.
                    // You can use it to access the Google API.
                    var token = result.credential;
                    // The signed-in user info.
                    var user = result.user;
                    console.log(token, user);
                    return user;
                }).catch(function (error) {
                    // Handle Errors here.
                    alert(error.message);
                    return error;
                });
            });
        }
    };
    FirebaseProvider.prototype.resetPassword = function (email) {
        return this.afAuth.auth.sendPasswordResetEmail(email);
    };
    FirebaseProvider.prototype.sendEmailVerificationLink = function (user) {
        var actionCodeSettings = { "url": "https://ionicmaster-7c02a.firebaseapp.com/" };
        return user.sendEmailVerification(actionCodeSettings);
    };
    FirebaseProvider.prototype.sendEmailLink = function (email) {
        var actionCodeSettings = { "url": "https://ionicmaster-7c02a.firebaseapp.com/" };
        return this.afAuth.auth.sendSignInLinkToEmail(email, actionCodeSettings);
    };
    FirebaseProvider.prototype.loginStatus = function () {
        var _this = this;
        this.afAuth.authState.subscribe(function (user) {
            if (user && user.emailVerified) {
                // this.firebaseProvider.setFreshUser(user);
            }
            else {
                _this.Superuser = null;
                _this.db_users = null;
                _this.main_users = null;
                _this.username = "";
                _this.isRegistredUser = false;
            }
        }, function (error) {
        });
    };
    FirebaseProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__["a" /* AngularFireAuth */]])
    ], FirebaseProvider);
    return FirebaseProvider;
}());

//# sourceMappingURL=firebase.js.map

/***/ }),

/***/ 93:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_firebase_firebase__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_status_bar__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__ = __webpack_require__(92);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HomePage = /** @class */ (function () {
    function HomePage(platform, nav, statusBar, splashScreen, firebaseProvider) {
        this.nav = nav;
        this.firebaseProvider = firebaseProvider;
        platform.ready().then(function () {
            statusBar.styleDefault();
            splashScreen.hide();
        });
        this.openPage();
    }
    HomePage.prototype.openPage = function () {
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/vaidahi/Desktop/KheloJiyoiOS/khelojiyoiosionic/src/pages/home/home.html"*/'<!-- <ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Ionic-Master</ion-title>\n  </ion-navbar>\n</ion-header> -->\n<ion-header>\n  <ion-navbar color="primary">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Ionic-Master</ion-title>\n    <ion-buttons end>\n      <button ion-button tappable>\n        <ion-icon name="notifications"></ion-icon>\n      </button>\n      <button ion-button tappable>\n        <ion-icon name="cog"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content  >\n    <div style="width: 100%;text-align: center; position: relative; ">\n\n      <video id="myVideo" width="100%" height="100%" autoplay="1" muted="">\n        <source src="https://cdn2.hubspot.net/hubfs/4234992/Cimconlighting-Feb2018/Videos/home-video-2.mp4?autoplay=1&mute=1&t=1534498051257" type="video/mp4">\n            Your browser does not support the video tag.</video>\n      <div style="position: absolute;\n                  top: 16px;\n                  position: absolute;\n                  left: 0;\n                  width: 100%;\n                  text-align: center;\n                  font-size: 18px;\n                  font-size: 18px;" >\n            <h1 style="color: #ffffff"  >\n                You should work with us!\n            </h1>\n            <h4 style="color: #ffffff">\n                XDuce is one of the emerging software companies located in Ahmedabad, India.<br> We offer mobile, web, customized software development and UI/UX design services.\n            </h4>\n          </div>\n\n\n  </div>\n\n</ion-content>\n'/*ion-inline-end:"/Users/vaidahi/Desktop/KheloJiyoiOS/khelojiyoiosionic/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* Platform */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_0__providers_firebase_firebase__["a" /* FirebaseProvider */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 96:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(165);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise__ = __webpack_require__(514);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_firebase_firebase__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_data_service_data_service__ = __webpack_require__(166);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// import {HttpParams} from '@angular/common/http/src/params';
// import { Component, ViewChild } from '@angular/core';
// import { Http, Headers,  } from '@angular/http';





// kimport {HttpParams} from '@angular/common/http/src/params';
//import { AlertController } from '../../../node_modules/ionic-angular';



var RegisterPage = /** @class */ (function () {
    function RegisterPage(navParams, navCtrl, http, firebaseProvider, api, url, loadingCtrl) {
        this.navParams = navParams;
        this.navCtrl = navCtrl;
        this.http = http;
        this.firebaseProvider = firebaseProvider;
        this.api = api;
        this.url = url;
        this.loadingCtrl = loadingCtrl;
        this.Firstname = "";
        this.Lastname = "";
        this.isCoach = true;
        this.Password = "";
        this.Password_2 = "";
        this.Email = "";
        this.Phone = "";
        this.u_email = "";
        this.flag = false;
        this.apiUrl = 'http://72.249.170.12/BluetoothApi/api/Login/Login';
        this.loading = this.loadingCtrl.create({ content: "please wait..." });
        //  if(navParams.get("email") && navParams.get("email")!=="null")  {
        //   debugger;
        //   this.u_email = this.firebaseProvider.Superuser.email.toLowerCase();
        //   this.Email = this.u_email;
        //   this.Password="null";
        //   if(this.u_email && this.u_email.length!==0){
        //     this.flag= true;
        //   }
        // }
    }
    RegisterPage.prototype.signup = function () {
        var _this = this;
        this.loading.present();
        var obj = { FirstName: this.Firstname,
            LastName: this.Lastname,
            Email: this.Email.toLowerCase(),
            Password: this.Password,
            RoleId: this.isCoach ? "1" : "2",
            PhoneNumber: this.Phone
        };
        this.api.postRequset(this.url.register, obj).subscribe(function (response_api) {
            _this.loading.dismiss();
            if (response_api.status === _this.constant.ok) {
                alert("SuccessToken: " + response_api.data);
            }
            else {
                alert("Failed: " + response_api.errors[0].code);
            }
        }, function (error) {
            _this.loading.dismiss();
            alert("error" + error);
        });
    };
    //https://www.djamware.com/post/59924f9080aca768e4d2b12e/ionic-3-consuming-rest-api-using-new-angular-43-httpclient
    // ionViewDidLoad(): void {
    //   setTimeout(() => {
    //     this.email.setFocus();
    //   }, 500);
    // }
    // register(): any {
    //   const obj = {
    //     UserName:this.Username ,
    //     Password:this.password,
    //     Name:this.Name,
    //     Email:this.Email
    //   }
    //   var headers = new Headers();
    //   headers.set('Content-type','application/json');
    //     console.log('doing'+ JSON.stringify(obj));
    //     debugger;
    //      const uri = 'http://72.249.170.12/BluetoothApi/api/Login/Login';
    //      this.http.post(uri,JSON.stringify(obj),{headers})
    //     .subscribe(res => {
    //       console.log("done");
    //       alert('done');
    //     }, (err) => {
    //       console.log(err);
    //     });
    // }
    // addUSer(){
    //   var obj : User;
    //    obj = new User();
    //    if(this.flag)
    //   {
    //     obj.Email= this.u_email;
    //     obj.LoginType= "gmail";
    //     obj.Password= "null";
    //   }
    //    else
    //   {
    //     obj.Email= this.u_email;
    //     obj.LoginType= "email";
    //     obj.Password= this.password;
    //   }
    //    obj.Name= this.Name;
    //    obj.Username= this.Username;
    //   this.firebaseProvider.addUser(obj);
    // }
    //{}
    // newSongRef.set({
    //   id: newSongRef.key,
    //   title: data.title
    // });
    // let prompt = this.alertCtrl.create({
    //   title: 'Submit the details',
    //   message: "Are you confirmed to submit ? ",
    //   inputs: [
    //     {
    //       name: 'title',
    //       placeholder: 'Title'
    //     },
    //   ],
    //   buttons: [
    //     {
    //       text: 'Cancel',
    //       handler: data => {
    //         console.log('Cancel clicked');
    //       }
    //     },
    //     {
    //       text: 'Save',
    //       handler: data => {
    //         const obj = {
    //           UserName:this.Username ,
    //           Password:this.password,
    //           Name:this.Name,
    //           Email:this.Email
    //         }
    //         const newSongRef = this.user.push(obj);
    //          console.log(""+newSongRef.key+","+data.title);
    //         // newSongRef.set({
    //         //   id: newSongRef.key,
    //         //   title: data.title
    //         // });
    //       }
    //     }
    //   ]
    // });
    // prompt.present();
    //}
    // signup(): void {
    //   var headers = new Headers();
    //   headers.set('Content-type', 'application/x-www-form-urlencoded');
    //   const uri = 'http://khelojiyo.com/api/users/register';
    //   this.http.post(uri, obj, { headers })
    //     .subscribe(res => {
    //       alert(res['_body']);
    //     }, (err) => {
    //       console.log(err);
    //     });
    // }
    // saveData(){
    //            var obj : User;
    //            obj = new User();
    //            if(this.flag)
    //             {
    //               obj.Email= this.u_email.toLowerCase();
    //               obj.LoginType= "gmail";
    //               obj.Password= "null";
    //             }
    //              else
    //             {
    //               obj.Email= this.Email.toLowerCase();
    //               obj.LoginType= "email";
    //               obj.Password= this.password;
    //             }
    //             obj.Name= this.firstname;
    //             obj.Username= this.lastname;
    //             this.firebaseProvider.addUser(obj).then(
    //               () => {
    //                 if(!this.flag)
    //                 {
    //                   alert("Please varifiy your email address.Check your mailbox for that.");
    //                   this.navCtrl.setRoot(LoginPage);
    //                 }
    //                 else{
    //                   this.navCtrl.setRoot(HomePage);
    //                 }
    //               },
    //               error => {
    //                 alert(error.message);
    //               }
    //             );
    //              //console.log("DataSaved:"+newSongRef.key+"--"+obj);
    //             // newSongRef.set({
    //             //   id: newSongRef.key,
    //             //   title: data.title
    //             // });
    // }
    RegisterPage.prototype.selectcoach = function (value) {
        this.isCoach = value;
    };
    RegisterPage.prototype.valid = function () {
        if (this.Firstname.length !== 0)
            return false;
        if (this.Lastname.length !== 0)
            return false;
        if (this.Email.length !== 0)
            return false;
        if (this.Phone.length !== 0)
            return false;
        if (this.Password.length !== 0)
            return false;
        if (this.Password_2.length !== 0)
            return false;
        if (this.Password !== this.Password_2)
            return false;
        else
            return true;
    };
    RegisterPage.prototype.finish = function () {
        this.navCtrl.pop();
    };
    RegisterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-register',template:/*ion-inline-start:"/Users/vaidahi/Desktop/KheloJiyoiOS/khelojiyoiosionic/src/pages/Register/register.html"*/'<ion-content class="login auth-page">\n  <div class="login-content">\n    <!-- Logo -->\n    <div>\n      <div class="logo"></div>\n    </div>\n    <ion-row>\n    </ion-row>\n    <div>\n      <form #loginForm="ngForm" autocomplete="off">\n        <ion-row>\n          <ion-col>\n            <ion-list inset>\n              <ion-item no-lines class="reg-input-block">\n                <ion-icon name="person" item-start class="text-grey"></ion-icon>\n                <ion-input text-center name="firstname" id="loginField" placeholder="FirstName" type="text" required\n                  [(ngModel)]=\'Firstname\' #email></ion-input>\n              </ion-item>\n              <ion-item no-lines class="reg-input-block">\n                <ion-icon name="person" item-start class="text-grey"></ion-icon>\n                <ion-input text-center name="lasttname" id="loginField" placeholder="LastName" type="text" required\n                  [(ngModel)]=\'Lastname\' #email></ion-input>\n              </ion-item>\n              <div class="wrapper-div">\n                <div [ngClass]="isCoach ? \'selected\' : \'deselected\'" class="left_item item-text-block"\n                  (click)=\'selectcoach(true)\'>\n                  <ion-icon name="person" item-start class="text-grey"></ion-icon>\n                  <span class="palinwhite-text">\n                    Coach\n                  </span>\n                </div>\n                <div [ngClass]="!isCoach ? \'selected\' : \'deselected\'" class="right_item item-text-block"\n                  (click)=\'selectcoach(false)\'>\n                  <ion-icon name="bicycle" item-start class="text-grey"></ion-icon>\n                  <span class="palinwhite-text">\n                    Player\n                  </span>\n                </div>\n              </div>\n              <ion-item no-lines class="reg-input-block">\n                <ion-icon name="call" item-start class="text-grey"></ion-icon>\n                <ion-input text-center name="phone" id="loginField" placeholder="Mobile Number" type="tel" required\n                  [(ngModel)]=\'Phone\' #number></ion-input>\n              </ion-item>\n              <ion-item no-lines class="reg-input-block">\n                <ion-icon name="mail" item-start class="text-grey"></ion-icon>\n                <ion-input text-center name="email" id="loginField" placeholder="play@khelojiyo.com" type="email"\n                  required [(ngModel)]=\'Email\' #email></ion-input>\n              </ion-item>\n\n              <ion-item no-lines class="reg-input-block">\n                <ion-icon name="key" item-start style="font-size:22px;" class="text-grey">\n                </ion-icon>\n                <ion-input text-center name="password" id="passwordField" \n                 [(ngModel)]=\'Password\' type="password"\n                  placeholder="Password" required></ion-input>\n\n              </ion-item>\n              <ion-item no-lines class="reg-input-block">\n                <ion-icon name="key" item-start style="font-size:22px;" class="text-grey">\n                </ion-icon>\n                <ion-input text-center name="password" id="passwordField" \n                [(ngModel)]=\'Password_2\' type="password"\n                  placeholder="Confirm Password" required></ion-input>\n\n              </ion-item>\n            </ion-list>\n          </ion-col>\n        </ion-row>\n        <ion-row style="margin-left:18px;\n                       margin-right:18px;">\n          <ion-col>\n            <div *ngIf="error" class="alert alert-danger">{{ error }}</div>\n            <button ion-button class="submit-btn" style="padding:22px;border-radius: 5px;" full type="submit" (click)="signup()"\n              [disabled]="!loginForm.form.valid">\n              REGISTER\n            </button>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n\n        </ion-row>\n      \n            <div style="text-align:center;margin:20px;\n                        color: white;font-size:12px;\n                        font-size:12px; \n                          font-style: italic;\n                       text-transform: initial;\n                       text-decoration: underline"\n                       (click)="finish()">\n              Back To Login\n            </div>\n               \n      </form>\n    </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/vaidahi/Desktop/KheloJiyoiOS/khelojiyoiosionic/src/pages/Register/register.html"*/
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
            __WEBPACK_IMPORTED_MODULE_4__providers_firebase_firebase__["a" /* FirebaseProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_data_service_data_service__["a" /* DataServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_data_service_data_service__["b" /* Url */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["f" /* LoadingController */]])
    ], RegisterPage);
    return RegisterPage;
}());

//# sourceMappingURL=register.js.map

/***/ })

},[319]);
//# sourceMappingURL=main.js.map